﻿// <copyright file="GameLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Tests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using YouAreBaba.Model;
    using YouAreBaba.Model.Objects;

    /// <summary>
    /// Testing GameLogic.
    /// </summary>
    [TestFixture]
    internal class GameLogicTests
    {
        /// <summary>
        /// Testing that if there is no rule on the map then the number of active rules is 0.
        /// </summary>
        [Test]
        public void ThereAreNoRulesOnAnEmptyMap_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(testMap, 0, 0);

            Assert.AreEqual(12 * 22, testMap.Count);
            Assert.AreEqual(0, gameLogic.Currentrulesofthegame.Count);
        }

        /// <summary>
        /// Testing that if there is only one rule on the map then the number of active rules is 1.
        /// </summary>
        [Test]
        public void ThereIsOnlyOneRuleOnTheMap_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(testMap, 0, 0);

            Assert.AreEqual(1, gameLogic.Currentrulesofthegame.Count);
        }

        /// <summary>
        /// Testing that if there are 3 rule on the map then the number of active rules is 3.
        /// </summary>
        [Test]
        public void ThereAreMultipleRulesOnTheMap_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // lesz egy szabály: FLAG IS WIN
                    else if (i == 4 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WIN));
                        }
                    }

                    // lesz egy szabály: WALL IS STOP
                    else if (i == 8 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WALL_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.STOP));
                        }
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(testMap, 0, 0);

            Assert.AreEqual(3, gameLogic.Currentrulesofthegame.Count);
        }

        /// <summary>
        /// Checking horizontal rule detection.
        /// </summary>
        [Test]
        public void HorizontalRuleDetection_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(testMap, 0, 0);

            Assert.AreEqual(MapObjectEnum.BABA_WORD, gameLogic.Currentrulesofthegame[0].WORD);
            Assert.AreEqual(MapObjectEnum.IS, gameLogic.Currentrulesofthegame[0].Operator);
            Assert.AreEqual(MapObjectEnum.YOU, gameLogic.Currentrulesofthegame[0].Property);
            Assert.AreEqual(VerticalHorizontalEnum.HORIZONTAL, gameLogic.Currentrulesofthegame[0].VHE);
        }

        /// <summary>
        /// Checking vertical rule detection.
        /// </summary>
        [Test]
        public void VerticalRuleDetection_Test()
        {
            List<MapObject> tesztPalya = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (j == 11 && (i == 4 || i == 5 || i == 6))
                    {
                        if (i == 4)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (i == 5)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (i == 6)
                        {
                            tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // a többi üres háttér
                    else
                    {
                        tesztPalya.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(tesztPalya, 0, 0);

            Assert.AreEqual(MapObjectEnum.BABA_WORD, gameLogic.Currentrulesofthegame[0].WORD);
            Assert.AreEqual(MapObjectEnum.IS, gameLogic.Currentrulesofthegame[0].Operator);
            Assert.AreEqual(MapObjectEnum.YOU, gameLogic.Currentrulesofthegame[0].Property);
            Assert.AreEqual(VerticalHorizontalEnum.VERTICAL, gameLogic.Currentrulesofthegame[0].VHE);
        }

        /// <summary>
        /// Checking that the rules can be pushed.
        /// </summary>
        [Test]
        public void RuleObjectBehaviour_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);
            gameLogic.MoveEveryMovableObject(testMap, 0, 0);

            // BABA_WORD
            Assert.AreEqual(MapObjectEnum.PUSH, testMap[141].RelevantBehaviors[0]);

            // IS
            Assert.AreEqual(MapObjectEnum.PUSH, testMap[142].RelevantBehaviors[0]);

            // YOU
            Assert.AreEqual(MapObjectEnum.PUSH, testMap[143].RelevantBehaviors[0]);
        }

        /// <summary>
        /// Checking defeat in case of elimination of SOMETHING IS YOU rules.
        /// </summary>
        [Test]
        public void DefeatInCaseOfEliminationOf_SOMETHING_IS_YOU_Rules_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // BABA
                    else if (i == 5 && j == 11)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            // BABA lefelé mozog egyet és a BABA IS YOU szabály YOU blokkját letolja, így mikor lefutna a következő mozgatás véget ér a játék
            gameLogic.MoveEveryMovableObject(testMap, 0, 1);

            Assert.AreEqual(1, gameLogic.MoveEveryMovableObject(testMap, 0, 1));
        }

        /// <summary>
        /// Checking pushing multiple mapobjects horizontally.
        /// </summary>
        [Test]
        public void PushingMultipleMapObjectsHorizontally_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // BABA
                    else if (i == 6 && j == 8)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            // eltolás előtti pozíciók
            Assert.AreEqual(MapObjectEnum.BABA, testMap[140].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, testMap[141].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, testMap[142].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, testMap[143].MyProperty);

            // BABA eltolja jobbra a horizontális szabályt
            gameLogic.MoveEveryMovableObject(testMap, 1, 0);

            // eltolás utáni pozíciók
            Assert.AreEqual(MapObjectEnum.BABA, testMap[141].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, testMap[142].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, testMap[143].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, testMap[144].MyProperty);
        }

        /// <summary>
        /// Checking pushing multiple mapobjects vertically.
        /// </summary>
        [Test]
        public void PushingMultipleMapObjectsVertically_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (j == 11 && (i == 4 || i == 5 || i == 6))
                    {
                        if (i == 4)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (i == 5)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (i == 6)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // BABA
                    else if (j == 11 && i == 3)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            // eltolás előtti pozíciók
            Assert.AreEqual(MapObjectEnum.BABA, testMap[77].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, testMap[99].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, testMap[121].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, testMap[143].MyProperty);

            // BABA eltolja lefelé a vertikális szabályt
            gameLogic.MoveEveryMovableObject(testMap, 0, 1);

            // eltolás utáni pozíciók
            Assert.AreEqual(MapObjectEnum.BABA, testMap[99].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA_WORD, testMap[121].MyProperty);
            Assert.AreEqual(MapObjectEnum.IS, testMap[143].MyProperty);
            Assert.AreEqual(MapObjectEnum.YOU, testMap[165].MyProperty);
        }

        /// <summary>
        /// Checking victory on a map.
        /// </summary>
        [Test]
        public void VictoryOnThisMap_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // lesz egy szabály: FLAG IS WIN
                    else if (i == 4 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.WIN));
                        }
                    }

                    // BABA
                    else if (i == 3 && j == 3)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // zászló
                    else if (i == 3 && j == 4)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.FLAG));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            Assert.AreEqual(2, gameLogic.MoveEveryMovableObject(testMap, 1, 0));
        }

        /// <summary>
        /// Jumping over adjacent mapobjects.
        /// </summary>
        [Test]
        public void Jump_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // BABA
                    else if (i == 3 && j == 3)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // GRASS
                    else if (i == 3 & (j == 4 || j == 5))
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.GRASS));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            // BABA egyet lépne jobbra, de mivel mellette közvetlen fű van, és az átugorható, ezért átugorja
            gameLogic.MoveEveryMovableObject(testMap, 1, 0);

            // a fű marad a helyén, BABA átugorja azt
            Assert.AreEqual(MapObjectEnum.GRASS, testMap[70].MyProperty);
            Assert.AreEqual(MapObjectEnum.GRASS, testMap[71].MyProperty);
            Assert.AreEqual(MapObjectEnum.BABA, testMap[72].MyProperty);
        }

        /// <summary>
        /// Testing that the frame stops.
        /// </summary>
        [Test]
        public void FrameStops_Test()
        {
            List<MapObject> testMap = new List<MapObject>();
            for (int i = 0; i < 12; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    // külső két sor keret
                    if (i < 2 || i > 9 || j < 2 || j > 19)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.KERET));
                    }

                    // lesz egy szabály: BABA IS YOU
                    else if (i == 6 && (j == 9 || j == 10 || j == 11))
                    {
                        if (j == 9)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA_WORD));
                        }

                        if (j == 10)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.IS));
                        }

                        if (j == 11)
                        {
                            testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.YOU));
                        }
                    }

                    // BABA a bal felső sarokban van a keret mellett közvetlen
                    else if (i == 2 && j == 2)
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.BABA));
                    }

                    // a többi üres háttér
                    else
                    {
                        testMap.Add(new MapObject(j * 36, i * 36, 36, 36, MapObjectEnum.EMPTY));
                    }
                }
            }

            GameLogic gameLogic = new GameLogic(22, 12, 36);

            // BABA egyet lépne balra, de mivel mellette közvetlen keret van, ami megállítja, ezért marad a helyén
            gameLogic.MoveEveryMovableObject(testMap, -1, 0);

            Assert.AreEqual(MapObjectEnum.BABA, testMap[46].MyProperty);
        }
    }
}
