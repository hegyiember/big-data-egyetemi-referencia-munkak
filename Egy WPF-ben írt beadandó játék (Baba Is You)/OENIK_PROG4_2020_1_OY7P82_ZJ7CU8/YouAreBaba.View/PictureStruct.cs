﻿// <copyright file="PictureStruct.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Holds the frames for a MapObject.
    /// </summary>
    internal class PictureStruct
    {
        private int currentsecondpart;

        /// <summary>
        /// Initializes a new instance of the <see cref="PictureStruct"/> class.
        /// </summary>
        /// <param name="gifPictureName">The name of the picture of the object.</param>
        /// <param name="maximumSecondPart">How many frames will it use.</param>
        public PictureStruct(string gifPictureName, int maximumSecondPart)
        {
            this.ObjectImageFrames = new List<BitmapImage>();
            this.GifPictureName = gifPictureName;
            this.currentsecondpart = 0;
            this.MaximumSecondPart = maximumSecondPart - 1;
        }

        /// <summary>
        /// Gets or sets all frames.
        /// </summary>
        public List<BitmapImage> ObjectImageFrames { get; set; }

        /// <summary>
        /// Gets or sets all frame names.
        /// </summary>
        public string GifPictureName { get; set; }

        /// <summary>
        /// Gets returns the cvurrent frame.
        /// </summary>
        public BitmapImage CurrentObjectImage
        {
            get { return this.ObjectImageFrames[this.CurrentSecondPart]; }
        }

        /// <summary>
        /// Gets which frame we are in.
        /// </summary>
        public int CurrentSecondPart
        {
            get { return this.currentsecondpart; }
        }

        /// <summary>
        /// Gets or sets the max frame number.
        /// </summary>
        public int MaximumSecondPart { get; set; }

        /// <summary>
        /// Increases the current frame.
        /// </summary>
        public void IncreaseCurrentSecondPart()
        {
            if (this.currentsecondpart < this.MaximumSecondPart)
            {
                this.currentsecondpart++;
            }
            else
            {
                this.currentsecondpart = 0;
            }
        }
    }
}
