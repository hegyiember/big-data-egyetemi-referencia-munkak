﻿// <copyright file="RulesAndControlsMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View.UserControls
{
    using System.Windows;
    using System.Windows.Controls;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for RulesAndControlsMenu.xaml.
    /// </summary>
    public partial class RulesAndControlsMenu : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RulesAndControlsMenu"/> class.
        /// </summary>
        public RulesAndControlsMenu()
        {
            this.InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = (1080 / 2) - 270;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = (1920 / 2) - 400;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
        }
    }
}
