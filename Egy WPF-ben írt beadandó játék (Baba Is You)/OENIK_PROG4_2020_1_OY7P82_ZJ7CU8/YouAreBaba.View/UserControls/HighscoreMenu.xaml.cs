﻿// <copyright file="HighscoreMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View.UserControls
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for HighscoreMenu.xaml.
    /// </summary>
    public partial class HighscoreMenu : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreMenu"/> class.
        /// </summary>
        public HighscoreMenu()
        {
            this.InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = (1080 / 2) - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = (1920 / 2) - 400;
            this.Liste.ItemsSource = (Application.Current.MainWindow.DataContext as MenuViewModel).Get().OrderByDescending(x => x.Mapid).ThenByDescending(y => y.Time);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 0;
        }
    }
}
