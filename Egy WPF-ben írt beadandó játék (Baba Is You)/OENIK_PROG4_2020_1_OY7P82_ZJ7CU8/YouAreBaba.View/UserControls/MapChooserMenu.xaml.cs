﻿// <copyright file="MapChooserMenu.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View.UserControls
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for MapChooserMenu.xaml.
    /// </summary>
    public partial class MapChooserMenu : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapChooserMenu"/> class.
        /// </summary>
        public MapChooserMenu()
        {
            this.InitializeComponent();
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionHeight = (1080 / 2) - 300;
            (Application.Current.MainWindow.DataContext as MenuViewModel).PositionWidth = (1920 / 2) - 450;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var mapid = (sender as Button).Content.ToString();
            if ((Application.Current.MainWindow.DataContext as MenuViewModel).CanThisMapBePlayedByUser(Convert.ToInt32(mapid)))
            {
                App.Current.Resources["CurrentMap"] = mapid;
                Console.WriteLine("MapChooserMenu SwitchView");
                (Window.GetWindow(this).DataContext as MenuViewModel).SwitchView = 1;
            }
        }
    }
}
