﻿// <copyright file="MusicFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// The main music provider.
    /// </summary>
    public class MusicFactory
    {
        private List<string> music;
        private System.Media.SoundPlayer player;
        private Random rnd;

        /// <summary>
        /// Initializes a new instance of the <see cref="MusicFactory"/> class.
        /// </summary>
        public MusicFactory()
        {
            this.player = new System.Media.SoundPlayer();
            this.music = new List<string>();
            this.rnd = new Random();
            this.ReadAllMusic();
        }

        /// <summary>
        /// Starts playing a random background music.
        /// </summary>
        public void PlayABackgroudnMusic()
        {
            this.player.SoundLocation = this.music[this.rnd.Next(0, this.music.Count - 1)];
            this.player.PlayLooping();
        }

        private void ReadAllMusic()
        {
            var directory = new DirectoryInfo(@"../../Music/background");
            foreach (var x in directory.GetFiles())
            {
                this.music.Add(x.FullName);
            }
        }
    }
}
