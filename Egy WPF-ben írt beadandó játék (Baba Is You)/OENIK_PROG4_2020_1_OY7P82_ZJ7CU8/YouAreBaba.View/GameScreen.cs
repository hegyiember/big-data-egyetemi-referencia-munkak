﻿// <copyright file="GameScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using YouAreBaba.Model;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// The main graphical class.
    /// </summary>
    public class GameScreen : FrameworkElement
    {
        private IGameViewModel gameModel;
        private DispatcherTimer timer;
        private DispatcherTimer timer2;
        private DispatcherTimer timer3;
        private Stopwatch timerall;
        private int temp;
        private int smootmovingtime;
        private int x;
        private int y;
        private bool hassmoothmovingended;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()
        {
            (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = false;
            this.temp = 0;
            this.IsKeyDown = 0;
            this.ActiveMapID = Convert.ToInt32(App.Current.Resources["CurrentMap"]);
            this.MapInformations = (Application.Current.MainWindow.DataContext as MenuViewModel).ScanMapSizes();

            this.ObjectPixel = 36;
            this.IamSpeed = 1;
            this.GameHeight = this.MapInformations[this.ActiveMapID][2];
            this.GameWidth = this.MapInformations[this.ActiveMapID][1];
            this.NumberOfUniformFrames = 3;
            this.FPSSpeed = 200;

            this.gameModel = new GameViewModel(this.GameWidth, this.GameHeight, this.ObjectPixel, this.ActiveMapID);
            this.Loaded += this.GameScreenLoaded;
            this.timer = new DispatcherTimer();
            this.timer2 = new DispatcherTimer();
            this.timer3 = new DispatcherTimer();
            this.timerall = new Stopwatch();
            this.timerall.Start();
            this.AllPictureNames = this.gameModel.ScanAllPictureNames();
            this.ObjectImages = new List<PictureStruct>();
            this.ConnectAllImages();
            Application.Current.MainWindow.Height = ((double)this.GameHeight * this.ObjectPixel) + 38;
            Application.Current.MainWindow.Width = ((double)this.GameWidth * this.ObjectPixel) + 14;

            this.smootmovingtime = 0;
            this.x = 0;
            this.y = 0;
            this.hassmoothmovingended = true;

            (Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad = false;
        }

        /// <summary>
        /// Gets or sets the dimensions of a MapObject.
        /// </summary>
        public int ObjectPixel { get; set; }

        /// <summary>
        /// Gets or sets the ID of currenty played map.
        /// </summary>
        public int ActiveMapID { get; set; }

        private int GameWidth { get; set; }

        private int GameHeight { get; set; }

        private int IamSpeed { get; set; }

        private int IsKeyDown { get; set; }

        private List<PictureStruct> ObjectImages { get; set; }

        private List<string> AllPictureNames { get; set; }

        private int NumberOfUniformFrames { get; set; }

        private int FPSSpeed { get; set; }

        private List<int[]> MapInformations { get; set; }

        /// <summary>
        /// Rendering graphical elements.
        /// </summary>
        /// <param name="drawingContext">The drawing instructions.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            List<MapObject> temp;
            if (this.smootmovingtime == 1)
            {
                temp = this.gameModel.PrevoiusmoveCurrentlyPlayedMap;
            }
            else
            {
                temp = this.gameModel.CurrentlyPlayedMap;
            }

            foreach (var inc in temp)
            {
                var giftemp = this.ObjectImages.Where(s => s.GifPictureName.Equals(inc.MyProperty.ToString())).Select(s => s.CurrentObjectImage).First();
                System.Windows.Rect kek = new System.Windows.Rect(inc.Area.X, inc.Area.Y, inc.Area.Width, inc.Area.Height);
                if (!inc.MyProperty.Equals(MapObjectEnum.EMPTY) && !inc.MyProperty.Equals(MapObjectEnum.KERET))
                {
                    drawingContext.DrawRectangle(new ImageBrush(giftemp), null, kek);
                }
            }
        }

        private void GameLogic_Death(object sender, EventArgs e)
        {
            (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
            Window.GetWindow(this).Close();
        }

        private void GameScreenLoaded(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("GameScreenLoaded");
            Application.Current.MainWindow.KeyDown += this.Window_KeyDown;
            Application.Current.MainWindow.KeyUp += this.Window_KeyUp;

            this.timer.Interval = TimeSpan.FromMilliseconds(this.IamSpeed); // 16
            this.timer.Tick += this.Timer_Tick;
            this.timer.Start();
            this.timer2.Interval = TimeSpan.FromMilliseconds(this.FPSSpeed); // FPSSpeed
            this.timer2.Tick += this.Timer2_Tick;
            this.timer2.Start();
            this.timer3.Interval = TimeSpan.FromMilliseconds(this.IamSpeed); // FPSSpeed
            this.timer3.Tick += this.Timer3_Tick;
            this.timer3.Start();
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            this.IsKeyDown = 0;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if ((this.x != 0 || this.y != 0) && this.smootmovingtime < this.ObjectPixel / 2)
            {
                this.hassmoothmovingended = false;
                this.smootmovingtime++;
                if (this.smootmovingtime != 0)
                {
                    this.gameModel.MoveSmoothlyPixels(this.x, this.y, this.smootmovingtime);
                }
            }
            else if (this.smootmovingtime >= this.ObjectPixel / 2)
            {
                this.smootmovingtime = 0;
                this.x = 0;
                this.y = 0;
                this.hassmoothmovingended = true;
                foreach (var xd in this.gameModel.CurrentlyPlayedMap)
                {
                    xd.RelevantBehaviors.Clear();
                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            foreach (var x in this.ObjectImages)
            {
                x.IncreaseCurrentSecondPart();
            }
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (this.IsKeyDown == 0)
            {
                Console.WriteLine("Window_KeyDown");
                switch (e.Key)
                {
                    case Key.Left: this.temp = this.SmoothMoving(-1, 0); break;
                    case Key.Right: this.temp = this.SmoothMoving(1, 0); break;
                    case Key.Up: this.temp = this.SmoothMoving(0, -1); break;
                    case Key.Down: this.temp = this.SmoothMoving(0, 1); break;
                    case Key.F5:
                        this.timer.Stop(); this.timer2.Stop(); this.timer3.Stop(); this.timerall.Stop();
                        this.gameModel.AddSaveGame();
                        MessageBox.Show("Game saved!");
                        (Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad = true;
                        this.timer.Start(); this.timer2.Start(); this.timer3.Start(); this.timerall.Start();
                        break;
                    case Key.F6:
                        if ((Application.Current.MainWindow.DataContext as MenuViewModel).CanLoad)
                        {
                            this.timer.Stop();
                            this.timer2.Stop();
                            this.timer3.Stop();
                            this.timerall.Stop();
                            this.gameModel.LoadGame();
                            MessageBox.Show("Game loaded!");
                            this.timer.Start();
                            this.timer2.Start();
                            this.timer3.Start();
                            this.timerall.Start();
                        }

                        break;
                    case Key.Escape:
                        Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                        Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                        this.timer.Stop();
                        this.timer2.Stop();
                        this.timerall.Stop();
                        MessageBox.Show("Exit!");
                        (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = this.ActiveMapID;
                        App.Current.Resources["CurrentMap"] = 0;
                        App.Current.Resources["CurrentMap"] = this.ActiveMapID;
                        this.gameModel = null;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                        (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                        break;
                }

                if (this.temp == 2)
                {
                    Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                    Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                    this.timer.Stop();
                    this.timer2.Stop();
                    this.timerall.Stop();
                    var elapsedtime = (int)this.timerall.Elapsed.TotalSeconds;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).Add(
                        (Application.Current.MainWindow.DataContext as MenuViewModel).GamerName,
                        this.ActiveMapID,
                        elapsedtime);

                    this.temp = 0;
                    MessageBox.Show("Winning!");
                    (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = this.ActiveMapID;
                    App.Current.Resources["CurrentMap"] = 0;
                    App.Current.Resources["CurrentMap"] = this.ActiveMapID;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeLastPlayedMapButtonToGreen();
                    this.gameModel = null;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                }
                else if (this.temp == 1)
                {
                    Application.Current.MainWindow.KeyDown -= this.Window_KeyDown;
                    Application.Current.MainWindow.KeyUp -= this.Window_KeyUp;
                    this.timer.Stop();
                    this.timer2.Stop();
                    this.timerall.Stop();
                    this.temp = 0;
                    MessageBox.Show("Losing!");
                    (this.DataContext as MenuViewModel).GameViewhasbeenloaded = false;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).LastPlayedMap = this.ActiveMapID;
                    App.Current.Resources["CurrentMap"] = 0;
                    App.Current.Resources["CurrentMap"] = this.ActiveMapID;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).ChangeLastPlayedMapButtonToRed();
                    this.gameModel = null;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).MenuOrGame = true;
                    (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 2;
                }
            }

            this.IsKeyDown++;
        }

        private int SmoothMoving(int x, int y)
        {
            if (this.hassmoothmovingended)
            {
                int kim = this.gameModel.Move(x, y);
                this.x = x;
                this.y = y;
                return kim;
            }
            else
            {
                return 0;
            }
        }

        private void ConnectAllImages()
        {
            foreach (var temp in this.AllPictureNames)
            {
                List<string> framesofthisgif = this.gameModel.ScanAllFramesOfGif(temp);
                this.ObjectImages.Add(new PictureStruct(temp, this.NumberOfUniformFrames));
                foreach (var x in framesofthisgif)
                {
                    this.ObjectImages.Last().ObjectImageFrames.Add(new BitmapImage(new Uri(@"../../Pictures/" + temp + "/" + x + ".png", UriKind.Relative)));
                }
            }
        }
    }
}
