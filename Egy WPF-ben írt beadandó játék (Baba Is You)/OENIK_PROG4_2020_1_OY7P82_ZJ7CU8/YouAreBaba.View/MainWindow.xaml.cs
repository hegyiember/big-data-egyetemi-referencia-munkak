﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.View
{
    using System;
    using System.Windows;
    using System.Windows.Input;
    using YouAreBaba.ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private MusicFactory musicFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            Console.WriteLine("MainWindow");
            this.musicFactory = new MusicFactory();
            //this.musicFactory.PlayABackgroudnMusic();
        }

        /// <summary>
        /// Gets the Music provider.
        /// </summary>
        public MusicFactory MusicFactory { get; }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView == 2 && e.Key.Equals(Key.Escape))
            {
                (Application.Current.MainWindow.DataContext as MenuViewModel).SwitchView = 0;
            }
        }
    }
}
