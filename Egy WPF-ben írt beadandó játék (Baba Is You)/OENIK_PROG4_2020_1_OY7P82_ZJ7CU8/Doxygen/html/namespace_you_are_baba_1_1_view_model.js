var namespace_you_are_baba_1_1_view_model =
[
    [ "GameViewModel", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html", "class_you_are_baba_1_1_view_model_1_1_game_view_model" ],
    [ "IGameViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model" ],
    [ "IMenuViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_menu_view_model.html", "interface_you_are_baba_1_1_view_model_1_1_i_menu_view_model" ],
    [ "IUserViewModel", "interface_you_are_baba_1_1_view_model_1_1_i_user_view_model.html", "interface_you_are_baba_1_1_view_model_1_1_i_user_view_model" ],
    [ "MenuViewModel", "class_you_are_baba_1_1_view_model_1_1_menu_view_model.html", "class_you_are_baba_1_1_view_model_1_1_menu_view_model" ],
    [ "UserViewModel", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html", "class_you_are_baba_1_1_view_model_1_1_user_view_model" ]
];