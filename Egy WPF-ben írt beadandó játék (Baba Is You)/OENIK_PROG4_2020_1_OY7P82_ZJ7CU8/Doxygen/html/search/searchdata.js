var indexSectionsWithContent =
{
  0: "01abcdefghiklmnoprstuvwxy",
  1: "acdfghimrsu",
  2: "xy",
  3: "01acdfghilmnprstuvy",
  4: "acdfghilmnoprsuv",
  5: "bcdefghiklmnoprstuwy",
  6: "mv",
  7: "bdefghiklmnoprstvwy",
  8: "abcdfghlmnoprstuvw",
  9: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "properties",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Properties",
  9: "Pages"
};

