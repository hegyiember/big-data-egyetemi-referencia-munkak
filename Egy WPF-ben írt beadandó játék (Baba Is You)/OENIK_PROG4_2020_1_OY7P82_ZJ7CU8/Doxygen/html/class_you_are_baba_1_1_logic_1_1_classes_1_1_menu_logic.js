var class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic =
[
    [ "MenuLogic", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a19cfd1bf276e2b33751718c5b99dbc0c", null ],
    [ "Add", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a81e5b9e6f9458b1041fdbced9d4aad17", null ],
    [ "CanThisMapBePlayedByUser", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a9dc05d908c9971bf97ee5751c9983b42", null ],
    [ "ChangeLastPlayedMapButtonToGreen", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a89a6e86f79910901493655c5796255b0", null ],
    [ "ChangeLastPlayedMapButtonToRed", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a7b1a7a61d38fc0c97b4b8a6a27c7aa95", null ],
    [ "ChangeMapButtonToGreen", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a23b5f94e548a65b48dd4f5547bdc6dbc", null ],
    [ "CheckUserExists", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a38c918b02410cbcb79919ca50898af75", null ],
    [ "CleanLastPlayedMap", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#abd1400e15b0e9e4181e0170af08b320e", null ],
    [ "DeleteAllRecord", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a57200383ca3caa36fe305a62a5be04be", null ],
    [ "Get", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#ae2d9c4c1bba85edee551968f843896a5", null ],
    [ "GetAllVictoryMapNumberByUser", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#a97ef62e1d647422db042bc812fbe49fe", null ],
    [ "ScanMapSizes", "class_you_are_baba_1_1_logic_1_1_classes_1_1_menu_logic.html#ab5c6639211cc2ed0273541a573b3f985", null ]
];