var dir_b4a30413794171d0c2f87c89fedf6056 =
[
    [ "ChoosePlayer.g.cs", "_choose_player_8g_8cs.html", [
      [ "ChoosePlayer", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player" ]
    ] ],
    [ "ChoosePlayer.g.i.cs", "_choose_player_8g_8i_8cs.html", [
      [ "ChoosePlayer", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_choose_player" ]
    ] ],
    [ "CurrentyPlayedGame.g.cs", "_currenty_played_game_8g_8cs.html", [
      [ "CurrentyPlayedGame", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game" ]
    ] ],
    [ "CurrentyPlayedGame.g.i.cs", "_currenty_played_game_8g_8i_8cs.html", [
      [ "CurrentyPlayedGame", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_currenty_played_game" ]
    ] ],
    [ "FrontMenu.g.cs", "_front_menu_8g_8cs.html", [
      [ "FrontMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu" ]
    ] ],
    [ "FrontMenu.g.i.cs", "_front_menu_8g_8i_8cs.html", [
      [ "FrontMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_front_menu" ]
    ] ],
    [ "HighscoreMenu.g.cs", "_highscore_menu_8g_8cs.html", [
      [ "HighscoreMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu" ]
    ] ],
    [ "HighscoreMenu.g.i.cs", "_highscore_menu_8g_8i_8cs.html", [
      [ "HighscoreMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_highscore_menu" ]
    ] ],
    [ "MapChooserMenu.g.cs", "_map_chooser_menu_8g_8cs.html", [
      [ "MapChooserMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu" ]
    ] ],
    [ "MapChooserMenu.g.i.cs", "_map_chooser_menu_8g_8i_8cs.html", [
      [ "MapChooserMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_map_chooser_menu" ]
    ] ],
    [ "RulesAndControlsMenu.g.cs", "_rules_and_controls_menu_8g_8cs.html", [
      [ "RulesAndControlsMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu" ]
    ] ],
    [ "RulesAndControlsMenu.g.i.cs", "_rules_and_controls_menu_8g_8i_8cs.html", [
      [ "RulesAndControlsMenu", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu.html", "class_you_are_baba_1_1_view_1_1_user_controls_1_1_rules_and_controls_menu" ]
    ] ]
];