var class_you_are_baba_1_1_view_model_1_1_game_view_model =
[
    [ "GameViewModel", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#ac46ac57a91878e6bc823b8e2ee8da0da", null ],
    [ "AddSaveGame", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a8ef98d3bdc507f4e5238357a23556dcc", null ],
    [ "GetCurrentlyPlayedMapObjectByCoordinates", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#ac339d2ecb0e3f82577e018deb87d634e", null ],
    [ "GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#aefdf278f1adbf64e46d9b0ac2bd76427", null ],
    [ "LoadGame", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a43845d1a0367bb3ec230a4ecb08001a6", null ],
    [ "Move", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#afbe685884b9b687ca1359eac870b096f", null ],
    [ "MoveSmoothlyPixels", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a0d9c75741b08bd9700e14d7a9268f257", null ],
    [ "ScanAllFramesOfGif", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#add97a2978471a32bd203542c3a61942b", null ],
    [ "ScanAllPictureNames", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a133df938936a663a84b1a24ddad4685d", null ],
    [ "AllMaps", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a1368d5d980c997f3bb0122d9e0640d99", null ],
    [ "CurrentlyPlayedMap", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a5ca5f79feb8e5da1e8c080cd99809fc0", null ],
    [ "CurrentMap", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#ac473f3c946bbf9fd3a68c87d085e7088", null ],
    [ "Logic", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#aeefc292ad7351b56e3e81e7b6d7cb527", null ],
    [ "MapObjectImages", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#ae68229722954bd427ce0f2940cc4cfa7", null ],
    [ "ObjectPixel", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#ad2a628c24dee905e69809ebf293710ae", null ],
    [ "PrevoiusmoveCurrentlyPlayedMap", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a4cfd934865a21c8cc46bc3561d941f75", null ],
    [ "Started", "class_you_are_baba_1_1_view_model_1_1_game_view_model.html#a935aba6dca835ab7cc699016acc1a62f", null ]
];