var interface_you_are_baba_1_1_view_model_1_1_i_game_view_model =
[
    [ "AddSaveGame", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a940783b44603c01d470dd21a506ea25a", null ],
    [ "GetCurrentlyPlayedMapObjectByCoordinates", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a02bb44e9e07a513f0aa04d92253d953c", null ],
    [ "GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a4f32c1ab1bc77a00da35188a9ce25f60", null ],
    [ "LoadGame", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#ac25f8a751257e4bedf7348a48fea9220", null ],
    [ "Move", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a94b7b85b084c2ceae122ed56d7e3ede5", null ],
    [ "MoveSmoothlyPixels", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a96ceafc51ebd694c40628998202d9036", null ],
    [ "ScanAllFramesOfGif", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#af0d98c2ebceb720af4bfcb5278c3fee9", null ],
    [ "ScanAllPictureNames", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#aa23bf7a638b09bb7f532fab02da7bcbc", null ],
    [ "CurrentlyPlayedMap", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a6380308f8de0f08406ef4d4e95006452", null ],
    [ "CurrentMap", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a1e3dcb620c3fe63ffda1fcb6172acffe", null ],
    [ "Logic", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#ab1e4d9d92be47a1d781769ab0f2b2c88", null ],
    [ "MapObjectImages", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a7857de648227cd8a1752da048e36ecc6", null ],
    [ "PrevoiusmoveCurrentlyPlayedMap", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a0f73b55a82de1592445daeb30d2ec97f", null ],
    [ "Started", "interface_you_are_baba_1_1_view_model_1_1_i_game_view_model.html#a4ac06f6e8a25fc70667283dd4f50a7db", null ]
];