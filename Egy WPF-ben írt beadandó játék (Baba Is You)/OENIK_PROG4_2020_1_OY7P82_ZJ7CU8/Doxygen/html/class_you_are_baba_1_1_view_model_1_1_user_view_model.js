var class_you_are_baba_1_1_view_model_1_1_user_view_model =
[
    [ "UserViewModel", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a485c3c0e1f06601e602836bafd70dba3", null ],
    [ "Add", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a7e9f3ca37b827b52df9a85a07389c399", null ],
    [ "CheckUserExists", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a8dd79498ffa6ddcbc079671a89460963", null ],
    [ "BoolchosenUser", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#abcc0ee6d209b6753a193dfe2841f8427", null ],
    [ "ChosenUser", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a18dfb91cf31b02b3a5e28e7dee3adc2b", null ],
    [ "FromListChosenUser", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#ab7b3bf76a5661e3e3e5ae3e9ecdc69e6", null ],
    [ "FromListChosenUserBool", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#ac5c7acebee7b6caa714a187a0d5933a3", null ],
    [ "Logic", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a4d21ada8dec0b22f300de66cf982c6b2", null ],
    [ "Usernames", "class_you_are_baba_1_1_view_model_1_1_user_view_model.html#a08b6ec98e8840524855e28e5831bcf72", null ]
];