﻿// <copyright file="UserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic.Classes
{
    using System.Collections.Generic;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;
    using YouAreBaba.Repositorys;

    /// <summary>
    /// Class responsible for the logic of the user.
    /// </summary>
    public class UserLogic : IUserLogic
    {
        private IDatabaseRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        public UserLogic()
        {
            this.repository = new DatabaseRepository();
        }

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        public bool Add(string name, int mapid, int time)
        {
            return this.repository.Add(name, mapid, time);
        }

        /// <summary>
        /// Checking if user exists.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>If exited true, otherwise false.</returns>
        public bool CheckUserExists(string name)
        {
            var listofusers = this.Get();
            foreach (var x in listofusers)
            {
                if (x.Name.Equals(name))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Collects all the names.
        /// </summary>
        /// <returns>A list if names.</returns>
        public List<string> GetOnlyNmes()
        {
            var tem = this.repository.Get();
            var liter = new List<string>();
            foreach (var x in tem)
            {
                if (!liter.Contains(x.Name))
                {
                    liter.Add(x.Name);
                }
            }

            return liter;
        }

        /// <summary>
        /// Get list of user data.
        /// </summary>
        /// <returns>The list of user data.</returns>
        private List<Highscore> Get()
        {
            return this.repository.Get();
        }
    }
}
