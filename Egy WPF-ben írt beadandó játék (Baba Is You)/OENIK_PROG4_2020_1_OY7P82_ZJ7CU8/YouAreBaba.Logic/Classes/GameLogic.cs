﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;
    using YouAreBaba.Model.Objects;
    using YouAreBaba.Repositorys;

    /// <summary>
    /// class responsible for the logic of the game.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private int width;
        private int height;
        private int objectPixel;
        private IRepository repository;
        private MapObjectEnum[] operatorEnumList;
        private MapObjectEnum[] propertiesEnumList;
        private MapObjectEnum[] wordEnumList;
        private MapObjectEnum[] notWordEnumList;
        private List<RuleObject> currentrulesofthegame;
        private List<int> theseshouldnotmove;
        private List<int> onlymoveone;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// constructor.
        /// </summary>
        /// <param name="width">how many fields the map width.</param>
        /// <param name="height">how many fields the map height.</param>
        /// <param name="objectPixel">how many pixels a field consists of.</param>
        public GameLogic(int width, int height, int objectPixel)
        {
            this.width = width;
            this.height = height;
            this.objectPixel = objectPixel;
            this.repository = new Repository(this.objectPixel, width, height);
            this.operatorEnumList = new MapObjectEnum[] { MapObjectEnum.IS /*, MapObjectEnum.HAS, MapObjectEnum.AND,  MapObjectEnum.MAKE, MapObjectEnum.ON, MapObjectEnum.NEAR */ };
            this.propertiesEnumList = new MapObjectEnum[] { MapObjectEnum.YOU, MapObjectEnum.WIN, MapObjectEnum.PUSH, /* MapObjectEnum.PULL, */ MapObjectEnum.STOP, MapObjectEnum.SINK, MapObjectEnum.DEFEAT, MapObjectEnum.MOVE, MapObjectEnum.SHUT, MapObjectEnum.OPEN, MapObjectEnum.WEAK, MapObjectEnum.SWAP };
            this.wordEnumList = new MapObjectEnum[] { MapObjectEnum.BABA_WORD, MapObjectEnum.EMPTY_WORD, MapObjectEnum.WALL_WORD, MapObjectEnum.WATER_WORD, MapObjectEnum.SKULL_WORD, MapObjectEnum.FENCE_WORD, MapObjectEnum.HEDGE_WORD, MapObjectEnum.TREE_WORD, MapObjectEnum.FLAG_WORD, MapObjectEnum.KEY_WORD, MapObjectEnum.GRASS_WORD, MapObjectEnum.KEKE_WORD, MapObjectEnum.ICE_WORD, MapObjectEnum.BELT_WORD, MapObjectEnum.BOX_WORD, MapObjectEnum.LEVEL_WORD, MapObjectEnum.LAVA_WORD, MapObjectEnum.DOOR_WORD, MapObjectEnum.ROCK_WORD };
            this.notWordEnumList = new MapObjectEnum[] { MapObjectEnum.BABA, MapObjectEnum.EMPTY, MapObjectEnum.WALL, MapObjectEnum.WATER, MapObjectEnum.SKULL, MapObjectEnum.FENCE, MapObjectEnum.HEDGE, MapObjectEnum.TREE, MapObjectEnum.FLAG, MapObjectEnum.KEY, MapObjectEnum.GRASS, MapObjectEnum.KEKE, MapObjectEnum.ICE, MapObjectEnum.BELT, MapObjectEnum.BOX, /* MapObjectEnum.LEVEL, */ MapObjectEnum.LAVA, MapObjectEnum.DOOR, MapObjectEnum.ROCK };
            this.currentrulesofthegame = new List<RuleObject>();
            this.theseshouldnotmove = new List<int>();
            this.onlymoveone = new List<int>();
        }

        /// <summary>
        /// Gets current rules of the game.
        /// </summary>
        public List<RuleObject> Currentrulesofthegame
        {
            get
            {
                return this.currentrulesofthegame;
            }
        }

        private List<MapObject> PreviousStateOfcpm { get; set; }

        /// <summary>
        /// Move smoothly pixels.
        /// </summary>
        /// <param name="cpm">Currently played map.</param>
        /// <param name="x">The x value of the move.</param>
        /// <param name="y">The y value of the move.</param>
        /// <param name="thismuchmoved">Array index of the examined MapObject.</param>
        public void MoveSmoothlyPixels(List<MapObject> cpm, int x, int y, int thismuchmoved)
        {
            for (int i = 0; i < cpm.Count(); i++)
            {
                if (
                    (
                    (cpm[i].RelevantBehaviors.Contains(MapObjectEnum.YOU)
                    || cpm[i].RelevantBehaviors.Contains(MapObjectEnum.MOVE))
                    && !cpm[i].RelevantBehaviors.Contains(MapObjectEnum.NEEDTOMOVE))
                    || cpm[i].RelevantBehaviors.Contains(MapObjectEnum.ONLYMOVEONCE))
                {
                    if (thismuchmoved == 1)
                    {
                        cpm[i].Move(-x * this.objectPixel, -y * this.objectPixel);
                    }

                    if (y == 0)
                    {
                        if (cpm[i].Area.X >= this.PreviousStateOfcpm[i].Area.X)
                        {
                            cpm[i].Move(x, y);
                            cpm[i].Move(x, y);
                        }
                        else if (cpm[i].Area.X <= this.PreviousStateOfcpm[i].Area.X)
                        {
                            cpm[i].Move(-x, y);
                            cpm[i].Move(-x, y);
                        }
                    }
                    else if (x == 0)
                    {
                        if (cpm[i].Area.Y >= this.PreviousStateOfcpm[i].Area.Y)
                        {
                            cpm[i].Move(x, y);
                            cpm[i].Move(x, y);
                        }
                        else if (cpm[i].Area.Y <= this.PreviousStateOfcpm[i].Area.Y)
                        {
                            cpm[i].Move(x, -y);
                            cpm[i].Move(x, -y);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves a map.
        /// </summary>
        /// <param name="currentMap">the id of the map to be saved.</param>
        /// <param name="currentlyPlayedMap">the map itself.</param>
        public void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap)
        {
            this.repository.AddSaveGame(currentMap, currentlyPlayedMap);
        }

        /// <summary>
        /// This method is responsible for moving movable fields.
        /// </summary>
        /// <param name="cpm">currently played map.</param>
        /// <param name="x">the x value of the move.</param>
        /// <param name="y">the y value of the move.</param>
        /// <returns>returns 1 if we die, returns 0 if we don't.</returns>
        public int MoveEveryMovableObject(List<MapObject> cpm, int x, int y)
        {
            // kiürítjük
            this.onlymoveone.Clear();
            this.theseshouldnotmove.Clear();

            // eltároljuk honnan lépünk a smooth mozgáshoz
            this.PreviousStateOfcpm = cpm;

            // beállítjuk a keretre, hogy megállítson, minden más push-olható
            foreach (var tree_check in cpm)
            {
                // amennyiben keretet találunk, akkor beállítjuk, hogy az megállítson
                if (tree_check.MyProperty.Equals(MapObjectEnum.KERET))
                {
                    tree_check.RelevantBehaviors.Add(MapObjectEnum.STOP);
                }

                // minden más alapból push-olható
                if (this.wordEnumList.Contains(tree_check.MyProperty) || this.propertiesEnumList.Contains(tree_check.MyProperty) || this.operatorEnumList.Contains(tree_check.MyProperty))
                {
                    tree_check.RelevantBehaviors.Add(MapObjectEnum.PUSH);
                }
            }

            // megkeressük és eltároljuk az összes szabályt
            for (int i = 0; i < cpm.Count - 1; i++)
            {
                if (this.operatorEnumList.Contains(cpm[i].MyProperty))
                {
                    if (i > 0 && i < ((this.height - 1) * this.width) - 2)
                    {
                        // vertikális szabályok megkeresése
                        if (this.wordEnumList.Contains(cpm[i - 1].MyProperty) && this.propertiesEnumList.Contains(cpm[i + 1].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - 1].MyProperty, cpm[i].MyProperty, cpm[i + 1].MyProperty, VerticalHorizontalEnum.HORIZONTAL);
                            Console.WriteLine("Vertikális Szabály hozzáadva: " + temper.WORD + " " + temper.Operator + " " + temper.Property);
                            this.currentrulesofthegame.Add(temper);
                        }
                    }

                    if (i > (this.width - 1) && i < (this.height - 1) * this.width)
                    {
                        // horizontális szabályok megkeresése
                        if (this.wordEnumList.Contains(cpm[i - this.width].MyProperty) && this.propertiesEnumList.Contains(cpm[i + this.width].MyProperty))
                        {
                            var temper = new RuleObject(cpm[i - this.width].MyProperty, cpm[i].MyProperty, cpm[i + this.width].MyProperty, VerticalHorizontalEnum.VERTICAL);
                            Console.WriteLine("Függőleges Szabály hozzáadva: " + temper.WORD + " " + temper.Operator + " " + temper.Property);
                            this.currentrulesofthegame.Add(temper);
                        }
                    }
                }
            }

            // a pályagrafikai elemekre megvizsgáljuk, hogy az aktuális szabályok közül épp melyik érvényes
            foreach (var currentobject in cpm)
            {
                // amennyiben egy olyan mezőt vizsgálunk, ami a pálya kirajzolásához kötődik
                if (this.notWordEnumList.Contains(currentobject.MyProperty))
                {
                    // megvizsgáljuk a jelenleg aktív szabályokat
                    foreach (var rule in this.currentrulesofthegame)
                    {
                        // ha a pályát nem befolyásoló tagja a szabálynak (1. tag) az egy grafikai mező típus
                        if (this.WORDtoObject(rule.WORD).Equals(currentobject.MyProperty))
                        {
                            // rögzítjük, hogy erre a mezőre milyen viselkedés vonatkozik
                            currentobject.RelevantBehaviors.Add(rule.Property);
                        }
                    }
                }
            }

            // beállítjuk, hogy azokon a mezőkön, amelyekre még nem vonatkozik szabály és nem a háttér vagy a szabályok részét képezik, át lehessen ugrani
            foreach (var obj in cpm)
            {
                // amennyiben az adott mezőre még nem vonatkozik semelyik szabály, nem háttér, nem egy szabály bármelyik része
                if (obj.RelevantBehaviors.Count == 0 && !obj.MyProperty.Equals(MapObjectEnum.EMPTY) && !this.operatorEnumList.Contains(obj.MyProperty) && !this.propertiesEnumList.Contains(obj.MyProperty) && !this.wordEnumList.Contains(obj.MyProperty))
                {
                    // beállítjuk ezekre a mezőkre, hogy át lehessen rajtuk ugrani
                    obj.RelevantBehaviors.Add(MapObjectEnum.WEAK);
                }
            }

            // YOU tulajdonság megvizsgálása = megnézzük, hogy élünk e még
            int howmanyofyoualiveyet = 0;
            foreach (var mek in cpm)
            {
                // az aktuális mező viselkedéseit vizsgáljuk
                foreach (var tex in mek.RelevantBehaviors)
                {
                    // ha tartalmaz YOU-t akkor "ezek vagyunk mi", ezt irányítjuk
                    if (tex.Equals(MapObjectEnum.YOU))
                    {
                        howmanyofyoualiveyet++;
                    }
                }
            }

            // ha semelyik mező nem rendelkezik a YOU tulajdonsággal, vagyis a játékos semmit nem irányít akkor vesztett
            if (howmanyofyoualiveyet < 1)
            {
                return this.DefeatOnThisMap(cpm);
            }

            // Mozgatás
            int indexofcpmnext = 0;
            int indexofcpmafternext = 0;
            List<int> theseareremovablebehaviorindexes = new List<int>();
            for (int i = 0; i < cpm.Count; i++)
            {
                // ha a mezőt még nem mozgattuk ebben a lépésben
                if (!cpm[i].MovedInThisTick)
                {
                    // Szépen meghatározzuk a mozgási irányban lévő kövi 2 MapObjectet (x vagy y irány)
                    indexofcpmnext = 0;
                    indexofcpmafternext = 0;

                    // a következő és az az utáni mezők indexének meghatározása
                    if (x == 0 && i > 2 * (this.width - 1) && i < (this.height - 2) * this.width)
                    {
                        indexofcpmnext = i + (y * this.width);
                        indexofcpmafternext = i + (y * this.width) + (y * this.width);
                    }
                    else if (y == 0 && i > 1 && i < (this.height * this.width) - 2)
                    {
                        indexofcpmnext = i + x;
                        indexofcpmafternext = i + x + x;
                    }

                    // ha az adott elem swappel akkor a következő is
                    // ugye swappoljuk mert a mozgó elem SWAPja bármit swappolhat
                    if (cpm[i].RelevantBehaviors.Contains(MapObjectEnum.SWAP) && !cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                    {
                        cpm[indexofcpmnext].RelevantBehaviors.Add(MapObjectEnum.SWAP);
                    }

                    // A WIN-t mindig a végére pakoljuk
                    // amennyiben a következő mező SWAP-pelhető és ezen felül, ha elérjük még győzünk is, akkor a WIN előtt legyen a SWAP a sorban, vagyis ha hozzáérünk, ne győzzünk, hanem helyet cseréljünk vele
                    if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.SWAP) && cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.WIN))
                    {
                        cpm[indexofcpmnext].RelevantBehaviors.Remove(MapObjectEnum.WIN);
                    }

                    // Nyitást teszünk vele.
                    if (
                       (
                       (
                       cpm[i].RelevantBehaviors.Contains(MapObjectEnum.YOU)
                       || cpm[i].RelevantBehaviors.Contains(MapObjectEnum.MOVE))
                       && cpm[i].RelevantBehaviors.Contains(MapObjectEnum.OPEN))
                       &&
                       (
                       cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.DOOR)
                       || cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.WATER)))
                    {
                        cpm[i].MyProperty = MapObjectEnum.EMPTY;
                        cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                    }

                    // Az adott mező viselkedésein végig kell mennünk, hogy rá melyik szabályokat kell alkalmaznunk
                    if (cpm[i].RelevantBehaviors.Count > 0)
                    {
                        foreach (var rule in cpm[i].RelevantBehaviors)
                        {
                            // csak azoknál a MapObjecteknél kell kezdeményezni mozgást, amelyek vagy te vagy vagy mozgató tulajdonsággal rendelkeznek
                            // tehát az irányított karaktert, mozgó tulajdonsággal rendelkező blokkokat és az ezek által eltolt blokkokak kell csak mozgatni, minden más marad a helyén
                            if (rule.Equals(MapObjectEnum.YOU) || rule.Equals(MapObjectEnum.MOVE))
                            {
                                // Lekezeljük amikor üres helyre lép át az objektum, hisz így nincsenek szabályok
                                // ha a következő mező egy háttér mező, akkor csak simán át kell oda mozgatni az adott objektumot
                                // karakter mozgatása a háttér mezőkön
                                if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                {
                                    // horizontális mozgás
                                    if (y == 0)
                                    {
                                        var imp = cpm[i].Dx;
                                        cpm[i].Move(x * this.objectPixel, 0);
                                        cpm[indexofcpmnext].Move(-1 * x * this.objectPixel, 0);
                                    }

                                    // vertikális mozgás
                                    else if (x == 0)
                                    {
                                        var imp = cpm[i].Dy;
                                        cpm[i].Move(0, y * this.objectPixel);
                                        cpm[indexofcpmnext].Move(0, -1 * y * this.objectPixel);
                                    }

                                    var temp = cpm[indexofcpmnext];
                                    cpm[indexofcpmnext] = cpm[i];
                                    cpm[i] = temp;
                                    cpm[indexofcpmnext].MoveInThisTick();
                                }

                                // ha következő mező, nem sima háttér mező akkor fut ez az ág
                                else
                                {
                                    // ha a következő mezőre nem vonatkozik semmilyen viselkedés, akkor kilépünk
                                    if (cpm[indexofcpmnext].RelevantBehaviors.Count < 1)
                                    {
                                        break;
                                    }

                                    // végigmegyünk a következő mező viselkedésein
                                    foreach (var nexttorule in cpm[indexofcpmnext].RelevantBehaviors)
                                    {
                                        switch (nexttorule)
                                        {
                                            case MapObjectEnum.STOP:
                                                this.theseshouldnotmove.Add(i);

                                                // semmi nem történik, hiszen nem mozdulhatsz ide!
                                                // amennyiben a következő mező megállít nem kell mozgatni semmit
                                                break;

                                            case MapObjectEnum.SHUT:
                                                this.theseshouldnotmove.Add(i);

                                                // semmi nem történik, hiszen nem mozdulhatsz ide!
                                                // amennyiben a következő mező "be van zárva" (az ajtós pálya), akkor nyílván ez is megállít és nem kell mozgatni semmit
                                                break;

                                            // ha a következő mező eltolható
                                            case MapObjectEnum.PUSH:
                                                // elképzelhető, hogy több mező van egymás mellett, ezt meg kell vizsgálni
                                                this.PushHelper(cpm, x, y, i, indexofcpmnext, indexofcpmafternext);

                                                // az utolsó mezo eltolása, amely után következő mező sima háttér
                                                if (cpm[indexofcpmnext].MyProperty.Equals(MapObjectEnum.EMPTY))
                                                {
                                                    if (y == 0)
                                                    {
                                                        cpm[indexofcpmnext].Move(-1 * x * this.objectPixel, 0);
                                                        cpm[i].Move(x * this.objectPixel, 0);
                                                    }
                                                    else if (x == 0)
                                                    {
                                                        cpm[indexofcpmnext].Move(0, -1 * y * this.objectPixel);
                                                        cpm[i].Move(0, y * this.objectPixel);
                                                    }

                                                    var tempnext = cpm[indexofcpmnext];
                                                    cpm[indexofcpmnext] = cpm[i];
                                                    cpm[i] = tempnext;
                                                    cpm[indexofcpmnext].MovedInThisTick = true;
                                                }
                                                else
                                                {
                                                    if (y == 0)
                                                    {
                                                        for (int gi = i; !cpm[gi].MyProperty.Equals(MapObjectEnum.KERET); gi += x)
                                                        {
                                                            this.onlymoveone.Remove(gi);
                                                            this.theseshouldnotmove.Add(gi);
                                                        }
                                                    }
                                                    else if (x == 0)
                                                    {
                                                        for (int gi = i; !cpm[gi].MyProperty.Equals(MapObjectEnum.KERET); gi += y * this.width)
                                                        {
                                                            this.onlymoveone.Remove(gi);
                                                            this.theseshouldnotmove.Add(gi);
                                                        }
                                                    }
                                                }

                                                break;

                                            // ha a következő mezőben elsüllyednél
                                            case MapObjectEnum.SINK:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                break;

                                            // ha a következő mezőben meghalnál
                                            case MapObjectEnum.DEFEAT:
                                                cpm[i].MyProperty = MapObjectEnum.EMPTY;
                                                theseareremovablebehaviorindexes.Add(i);
                                                howmanyofyoualiveyet--;
                                                if (howmanyofyoualiveyet < 1)
                                                {
                                                    return this.DefeatOnThisMap(cpm);
                                                }

                                                break;

                                            // ha következő mezővel kicserélődnél
                                            case MapObjectEnum.SWAP:
                                                if (y == 0)
                                                {
                                                    cpm[i].Move(x * this.objectPixel, 0);
                                                    cpm[indexofcpmnext].Move(-1 * x * this.objectPixel, 0);
                                                }
                                                else if (x == 0)
                                                {
                                                    cpm[i].Move(0, y * this.objectPixel);
                                                    cpm[indexofcpmnext].Move(0, -1 * y * this.objectPixel);
                                                }

                                                var temper = cpm[indexofcpmnext];
                                                cpm[indexofcpmnext] = cpm[i];
                                                cpm[i] = temper;
                                                cpm[i].MovedInThisTick = true;
                                                cpm[indexofcpmnext].MovedInThisTick = true;
                                                break;

                                            // amennyiben át lehet ugrani mezőkön
                                            case MapObjectEnum.WEAK:
                                                var foundempty = i;
                                                if (y == 0)
                                                {
                                                    foundempty = this.JumpHelper(cpm, x, y, i + x);
                                                }
                                                else if (x == 0)
                                                {
                                                    foundempty = this.JumpHelper(cpm, x, y, i + (y * this.width));
                                                }

                                                if (foundempty != -1)
                                                {
                                                    if (y == 0)
                                                    {
                                                        cpm[i].Move((foundempty - i) * this.objectPixel, 0);
                                                        cpm[foundempty].Move((-1 * (foundempty - i)) * this.objectPixel, 0);
                                                    }
                                                    else if (x == 0)
                                                    {
                                                        cpm[i].Move(0, (foundempty - i) / this.width * this.objectPixel);
                                                        cpm[foundempty].Move(0, (-1 * (foundempty - i)) / this.width * this.objectPixel);
                                                    }

                                                    var temp = cpm[foundempty];
                                                    cpm[foundempty] = cpm[i];
                                                    cpm[i] = temp;
                                                    cpm[foundempty].MoveInThisTick();

                                                    // azokat a mezőket, amelyek felett átugrottunk, már nem kell mozgatni
                                                    for (int ij = foundempty; ij > i; ij--)
                                                    {
                                                        cpm[ij].MoveInThisTick();
                                                    }
                                                }
                                                else
                                                {
                                                    this.theseshouldnotmove.Add(i);
                                                }

                                                break;
                                            case MapObjectEnum.WIN: return this.VictoryOnThisMap();

                                            default:
                                                this.theseshouldnotmove.Add(i);
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // A játék közben fellépő viselkedési törléseket (SINK, DEFEAT) kell itt lekezelni. A végén persze minden behaviour törlésre kerül, hogy az új Move esetén töltődjön.
                    foreach (var xe in theseareremovablebehaviorindexes)
                    {
                        cpm[xe].RelevantBehaviors.Clear();
                    }
                }
            }

            // visszaállítjuk az összes mezőt, hogy a következő lépésben újra lehessen őket mozgatni
            foreach (var xe in cpm)
            {
                xe.NotMoveInThisTick();
            }

            // az összes jelenleg aktív szabályt töröljük, hiszen a következő lépésben, már új szabályok lesznek érvényben
            this.currentrulesofthegame.Clear();

            // a pixelmozgatáshoz cpm-be pakoljuk a NEEDTOMOVE-t
            foreach (var xe in this.theseshouldnotmove)
            {
                if (!cpm[xe].MyProperty.Equals(MapObjectEnum.KERET))
                {
                    cpm[xe].RelevantBehaviors.Add(MapObjectEnum.NEEDTOMOVE);
                }
            }

            foreach (var xe in this.onlymoveone)
            {
                if (!cpm[xe].MyProperty.Equals(MapObjectEnum.KERET))
                {
                    cpm[xe].RelevantBehaviors.Add(MapObjectEnum.ONLYMOVEONCE);
                }
            }

            return 0;
        }

        /// <summary>
        /// Refreshing mapmaking.
        /// </summary>
        /// <param name="currentlyPlayedMap">Currently played map.</param>
        /// <returns>Returns the map in a string matrix.</returns>
        public string[,] NewTickSoFreshMapMaking(List<MapObject> currentlyPlayedMap)
        {
            var kimenet = new string[this.height, this.width];
            foreach (var viewedmapobject in currentlyPlayedMap)
            {
                kimenet[viewedmapobject.Dy / this.objectPixel, viewedmapobject.Dx / this.objectPixel] = Enum.GetName(typeof(MapObjectEnum), viewedmapobject.MyProperty);
            }

            return kimenet;
        }

        /// <summary>
        /// Scans all frames of gif.
        /// </summary>
        /// <param name="temp">Current name of field.</param>
        /// <returns>Collects the names of the animation frames of the field passed in the string into a string array.</returns>
        public List<string> ScanAllFramesOfGif(string temp)
        {
            return this.repository.ScanAllFramesOfGif(temp);
        }

        /// <summary>
        /// This method loads all the maps from the Maps folder.
        /// </summary>
        /// <returns>Returns all the maps.</returns>
        public List<List<MapObject>> ScanAllMaps()
        {
            return this.repository.ScanAllMaps();
        }

        /// <summary>
        /// Collects the names of the pictures from the Pictures folder, where the appearance of each field is stored.
        /// </summary>
        /// <returns>All pictures names.</returns>
        public List<string> ScanAllPictureNames()
        {
            return this.repository.ScanAllPictureNames();
        }

        /// <summary>
        /// Victory on this map.
        /// </summary>
        /// <returns>2.</returns>
        public int VictoryOnThisMap()
        {
            // Majd visszadob a pályaválasztó menübe, miközben elment minden releváns infót;
            return 2;
        }

        /// <summary>
        /// Quicksave function.
        /// </summary>
        /// <returns>A list of MapObjects.</returns>
        public List<MapObject> LoadGame()
        {
            return this.repository.LoadGame();
        }

        private int DefeatOnThisMap(List<MapObject> cpm)
        {
            // Irány vissza a menübe! Nem mentünk vereséget, hisza a map megnyitása előtti állapot az érvényes
            return 1;
        }

        private void PushHelper(List<MapObject> cpm, int x, int y, int i, int indexofcpmnext, int indexofcpmafternext)
        {
            if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.OPEN)
               && (cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.DOOR)
               || cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.WATER)))
            {
                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                cpm[indexofcpmafternext].MyProperty = MapObjectEnum.EMPTY;
            }

            // ha a kovetkezo utani mezo is eltolhato, vagy a következő mező szabály blokk tovább kell vizsgálnunk a következő mezőket
            if (cpm[indexofcpmafternext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) || this.wordEnumList.Contains(cpm[indexofcpmnext].MyProperty) || this.propertiesEnumList.Contains(cpm[indexofcpmnext].MyProperty) || this.operatorEnumList.Contains(cpm[indexofcpmnext].MyProperty))
            {
                this.onlymoveone.Add(indexofcpmafternext);
                if (y == 0)
                {
                    this.PushHelper(cpm, x, y, i + x, indexofcpmnext + x, indexofcpmafternext + x);
                }
                else if (x == 0)
                {
                    this.PushHelper(cpm, x, y, i + (y * this.width), indexofcpmnext + (y * this.width), indexofcpmafternext + (y * this.width));
                }
            }

            // ha a következő mező eltolható és a következő utáni mezőben elsüllyedsz
            // ez az az eset amikor egy követ beletolunk a vízbe
            if (cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) && cpm[indexofcpmafternext].RelevantBehaviors.Contains(MapObjectEnum.SINK))
            {
                cpm[indexofcpmnext].MyProperty = MapObjectEnum.EMPTY;
                cpm[indexofcpmafternext].MyProperty = MapObjectEnum.EMPTY;
            }

            // ha az aktuális mező nem háttér mező és a következő mező eltolható és a következő utáni mező is háttér
            // ez tehát az az eset amennyiben valamilyen blokkal eltolunk egy másik blokkot, mert a rákövetkező mező üres és ezért megtehetjük
            if (!cpm[i].MyProperty.Equals(MapObjectEnum.EMPTY) && cpm[indexofcpmnext].RelevantBehaviors.Contains(MapObjectEnum.PUSH) && cpm[indexofcpmafternext].MyProperty.Equals(MapObjectEnum.EMPTY))
            {
                if (y == 0)
                {
                    cpm[indexofcpmafternext].Move(-1 * x * this.objectPixel, 0);
                    cpm[indexofcpmnext].Move(x * this.objectPixel, 0);
                }
                else if (x == 0)
                {
                    cpm[indexofcpmafternext].Move(0, -1 * y * this.objectPixel);
                    cpm[indexofcpmnext].Move(0, y * this.objectPixel);
                }

                var tempnext = cpm[indexofcpmafternext];
                cpm[indexofcpmafternext] = cpm[indexofcpmnext];
                cpm[indexofcpmnext] = tempnext;
                cpm[indexofcpmafternext].MovedInThisTick = true;
                this.onlymoveone.Add(indexofcpmafternext);
            }
        }

        private int JumpHelper(List<MapObject> cpm, int x, int y, int i)
        {
            // ha találunk egy háttér mezőt, amire átugorhatunk, akkor visszaadjuk az indexét
            if (cpm[i].MyProperty.Equals(MapObjectEnum.EMPTY))
            {
                return i;
            }

            // ha az aktuális mező egy keret mező vagy szabály blokk, akkor nem ugorhatunk át ide/rajta keresztül
            else if (cpm[i].MyProperty.Equals(MapObjectEnum.KERET) || this.wordEnumList.Contains(cpm[i].MyProperty) || this.propertiesEnumList.Contains(cpm[i].MyProperty) || this.operatorEnumList.Contains(cpm[i].MyProperty))
            {
                return -1;
            }

            // ha ezektől eltérő típusú az aktuális mező, akkor folytatjuk a vizsgálatot
            else
            {
                if (y == 0)
                {
                    return this.JumpHelper(cpm, x, y, i + x);
                }
                else
                {
                    return this.JumpHelper(cpm, x, y, i + (y * this.width));
                }
            }
        }

        private MapObjectEnum WORDtoObject(MapObjectEnum wORD)
        {
            switch (wORD)
            {
                case MapObjectEnum.BABA_WORD: return MapObjectEnum.BABA;
                case MapObjectEnum.EMPTY_WORD: return MapObjectEnum.EMPTY;
                case MapObjectEnum.WALL_WORD: return MapObjectEnum.WALL;
                case MapObjectEnum.WATER_WORD: return MapObjectEnum.WATER;
                case MapObjectEnum.SKULL_WORD: return MapObjectEnum.SKULL;
                case MapObjectEnum.FENCE_WORD: return MapObjectEnum.FENCE;
                case MapObjectEnum.HEDGE_WORD: return MapObjectEnum.HEDGE;
                case MapObjectEnum.TREE_WORD: return MapObjectEnum.TREE;
                case MapObjectEnum.FLAG_WORD: return MapObjectEnum.FLAG;
                case MapObjectEnum.KEY_WORD: return MapObjectEnum.KEY;
                case MapObjectEnum.GRASS_WORD: return MapObjectEnum.GRASS;
                case MapObjectEnum.KEKE_WORD: return MapObjectEnum.KEKE;
                case MapObjectEnum.ICE_WORD: return MapObjectEnum.ICE;
                case MapObjectEnum.BELT_WORD: return MapObjectEnum.BELT;
                case MapObjectEnum.BOX_WORD: return MapObjectEnum.BOX;
                case MapObjectEnum.LEVEL_WORD: return MapObjectEnum.LEVEL_WORD;
                case MapObjectEnum.LAVA_WORD: return MapObjectEnum.LAVA;
                case MapObjectEnum.DOOR_WORD: return MapObjectEnum.DOOR;
                case MapObjectEnum.ROCK_WORD: return MapObjectEnum.ROCK;
            }

            return wORD;
        }
    }
}
