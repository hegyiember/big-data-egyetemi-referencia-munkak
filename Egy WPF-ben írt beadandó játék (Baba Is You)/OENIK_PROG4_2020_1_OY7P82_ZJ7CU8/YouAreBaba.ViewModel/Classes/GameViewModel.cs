﻿// <copyright file="GameViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using YouAreBaba.Logic;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;

    /// <summary>
    /// The main GameView Model class.
    /// </summary>
    public class GameViewModel : IGameViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameViewModel"/> class.
        /// </summary>
        /// <param name="width">The width of the map.</param>
        /// <param name="height">The height of the map.</param>
        /// <param name="objectPixel">Dimension of MapObject.</param>
        /// <param name="currentmap">The ID of the currently played map.</param>
        public GameViewModel(int width, int height, int objectPixel, int currentmap)
        {
            this.ObjectPixel = objectPixel;
            this.Logic = new GameLogic(width, height, this.ObjectPixel);
            this.AllMaps = this.Logic.ScanAllMaps();
            this.CurrentlyPlayedMap = this.AllMaps[currentmap];
            this.PrevoiusmoveCurrentlyPlayedMap = this.CurrentlyPlayedMap;
            this.MapObjectImages = new List<BitmapImage>();
            this.Started = DateTime.Now;
            this.CurrentMap = currentmap;
        }

        /// <inheritdoc/>
        public IGameLogic Logic { get; set; }

        /// <summary>
        /// Gets or sets all possible maps.
        /// </summary>
        public List<List<MapObject>> AllMaps { get; set; }

        /// <inheritdoc/>
        public List<MapObject> CurrentlyPlayedMap { get; set; }

        /// <inheritdoc/>
        public List<MapObject> PrevoiusmoveCurrentlyPlayedMap { get; set; }

        /// <inheritdoc/>
        public List<BitmapImage> MapObjectImages { get; set; }

        /// <inheritdoc/>
        public DateTime Started { get; set; }

        /// <inheritdoc/>
        public int CurrentMap { get; set; }

        private int ObjectPixel { get; set; }

        /// <inheritdoc/>
        public void AddSaveGame()
        {
            this.Logic.AddSaveGame(this.CurrentMap, this.CurrentlyPlayedMap);
        }

        /// <inheritdoc/>
        public void LoadGame()
        {
            var temp = this.Logic.LoadGame();
            for (int inx = 0; inx < this.CurrentlyPlayedMap.Count(); inx++)
            {
                this.CurrentlyPlayedMap[inx] = temp[inx];
            }
        }

        /// <inheritdoc/>
        public int Move(int x, int y)
        {
            this.PrevoiusmoveCurrentlyPlayedMap = this.CurrentlyPlayedMap;
            return this.Logic.MoveEveryMovableObject(this.CurrentlyPlayedMap, x, y);
        }

        /// <inheritdoc/>
        public void MoveSmoothlyPixels(int x, int y, int thismuchmoved)
        {
            this.Logic.MoveSmoothlyPixels(this.CurrentlyPlayedMap, x, y, thismuchmoved);
        }

        /// <inheritdoc/>
        public List<string> ScanAllPictureNames()
        {
            return this.Logic.ScanAllPictureNames();
        }

        /// <inheritdoc/>
        public Rect GetCurrentlyPlayedMapObjectByCoordinates(int i, int j)
        {
            foreach (var s in this.CurrentlyPlayedMap)
            {
                if (s.Dx == i * this.ObjectPixel && s.Dy == j * this.ObjectPixel)
                {
                    return s.Area;
                }
            }

            return new Rect(0, 0, this.ObjectPixel, this.ObjectPixel);
        }

        /// <inheritdoc/>
        public string GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates(int i, int j)
        {
            foreach (var s in this.CurrentlyPlayedMap)
            {
                if (s.Dx == i * this.ObjectPixel && s.Dy == j * this.ObjectPixel)
                {
                    return s.MyProperty.ToString();
                }
            }

            return "EMPTY";
        }

        /// <inheritdoc/>
        public List<string> ScanAllFramesOfGif(string temp)
        {
            return this.Logic.ScanAllFramesOfGif(temp);
        }
    }
}
