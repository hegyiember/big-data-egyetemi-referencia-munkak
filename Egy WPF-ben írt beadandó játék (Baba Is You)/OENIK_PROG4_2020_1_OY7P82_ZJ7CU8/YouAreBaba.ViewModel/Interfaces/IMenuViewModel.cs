﻿// <copyright file="IMenuViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;

    /// <summary>
    /// The menu view model interface.
    /// </summary>
    public interface IMenuViewModel
    {
        /// <summary>
        /// Gets or sets userControl ID currently displayed.
        /// </summary>
        int SwitchView { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the game can only works if this is true.
        /// </summary>
        bool GameViewhasbeenloaded { get; set; }

        /// <summary>
        /// Gets logo picture in the menu.
        /// </summary>
        BitmapImage Logo { get; }

        /// <summary>
        /// Gets Background picture in the menu.
        /// </summary>
        BitmapImage Back { get; }

        /// <summary>
        /// Gets helper picture in the menu.
        /// </summary>
        BitmapImage Help { get; }

        /// <summary>
        /// Gets or sets all maps the user has already finished.
        /// </summary>
        List<int> FromSavedGameFinishedMap { get; set; }

        /// <summary>
        /// Gets or sets the last played map by the user.
        /// </summary>
        int LastPlayedMap { get; set; }

        /// <summary>
        /// Gets or sets the buttons of the map.
        /// </summary>
        ObservableCollection<FluidButton> MyButtons { get; set; }

        /// <summary>
        /// Gets or sets the currenly playing user name.
        /// </summary>
        string GamerName { get; set; }

        /// <summary>
        /// Gets main logic connection.
        /// </summary>
        IMenuLogic Logic { get; }

        /// <summary>
        /// Gets or sets a value indicating whether enables or disables quicksave function. Prevents earlier map saves.
        /// </summary>
        /// <returns>Can you save or not.</returns>
        bool CanLoad { get; set; }

        /// <summary>
        /// Gets or sets y position of UserControl.
        /// </summary>
        int PositionHeight { get; set; }

        /// <summary>
        /// Gets or sets x mposition of UserControl.
        /// </summary>
        int PositionWidth { get; set; }

        /// <summary>
        /// Set the USerControl dimensions when a map is started.
        /// </summary>
        /// <param name="mapid">The played map ID.</param>
        /// <param name="objectPixel">MapObject dimensions.</param>
        void SetMapForGameplay(int mapid, int objectPixel);

        /// <summary>
        /// As the name says.
        /// </summary>
        void ChangeLastPlayedMapButtonToRed();

        /// <summary>
        /// As the name says.
        /// </summary>
        void ChangeLastPlayedMapButtonToGreen();

        /// <summary>
        /// As the name says.
        /// </summary>
        void ChangeVictoriousPlayedMapButtonToGreen();

        /// <summary>
        /// As the name says.
        /// </summary>
        void ChangeAllPlayedMapButtonToNothing();

        /// <summary>
        /// Scan all map dimensions.
        /// </summary>
        /// <returns>List of map dimensions.</returns>
        List<int[]> ScanMapSizes();

        /// <summary>
        /// Get a list of highscores.
        /// </summary>
        /// <returns>List of highscores.</returns>
        List<Highscore> Get();

        /// <summary>
        /// Add a new highscore.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <param name="mapid">ID of played map.</param>
        /// <param name="time">Time spent on map.</param>
        /// <returns>Was it succesfull or not.</returns>
        bool Add(string name, int mapid, int time);

        /// <summary>
        /// Delete every highscore of a user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>Was it succesfull or not.</returns>
        bool DeleteAllRecord(string name);

        /// <summary>
        /// Gat every map ID that the user alreayd finished.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>List of done mapids.</returns>
        List<int> GetAllVictoryMapNumberByUser(string name);

        /// <summary>
        /// Chech user existence.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>User exists or not.</returns>
        bool CheckUserExists(string name);
    }
}
