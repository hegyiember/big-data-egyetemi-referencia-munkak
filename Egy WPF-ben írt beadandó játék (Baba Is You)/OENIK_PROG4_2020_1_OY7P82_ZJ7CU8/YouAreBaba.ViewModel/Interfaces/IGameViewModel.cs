﻿// <copyright file="IGameViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Media.Imaging;
    using YouAreBaba.Logic.Interfaces;
    using YouAreBaba.Model;

    /// <summary>
    /// View Model for a currently played map.
    /// </summary>
    public interface IGameViewModel
    {
        /// <summary>
        /// Gets or sets main logic connection.
        /// </summary>
        IGameLogic Logic { get; set; }

        /// <summary>
        /// Gets or sets the currently played map.
        /// </summary>
        List<MapObject> CurrentlyPlayedMap { get; set; }

        /// <summary>
        /// Gets or sets the current map before player moved. Used for Smooth Moving. For a frame it seth the map back before it moved and then moves.
        /// </summary>
        List<MapObject> PrevoiusmoveCurrentlyPlayedMap { get; set; }

        /// <summary>
        /// Gets or sets all image resources for MapObjects.
        /// </summary>
        List<BitmapImage> MapObjectImages { get; set; }

        /// <summary>
        /// Gets or sets how long the player finished the map.
        /// </summary>
        DateTime Started { get; set; }

        /// <summary>
        /// Gets or sets iD of the currently played map.
        /// </summary>
        int CurrentMap { get; set; }

        /// <summary>
        /// Add a new quicksave.
        /// </summary>
        void AddSaveGame();

        /// <summary>
        /// Load a quicksave.
        /// </summary>
        void LoadGame();

        /// <summary>
        /// Main loggic, Move all MapObject elements and apply rules.
        /// </summary>
        /// <param name="v1">X dimension.</param>
        /// <param name="v2">Y dimension.</param>
        /// <returns>Victory, lose or just quit.</returns>
        int Move(int v1, int v2);

        /// <summary>
        /// Other main method, runs after Move(), smoothy moves every moved MapObject in the Tick.
        /// </summary>
        /// <param name="x">X dimension.</param>
        /// <param name="y">Y dimension.</param>
        /// <param name="thismuchmoved">The ID of the MapObcect we want to move.</param>
        void MoveSmoothlyPixels(int x, int y, int thismuchmoved);

        /// <summary>
        /// Scann all possible picture names for MapObjects to get the name of pictores.
        /// </summary>
        /// <returns>A list of possible picture names.</returns>
        List<string> ScanAllPictureNames();

        /// <summary>
        /// Get the Rect object from a MapObject by coodinates to draw it out.
        /// </summary>
        /// <param name="i">X dimension.</param>
        /// <param name="j">Y dimension.</param>
        /// <returns>The Windows.System.Rect of the Mapobject.</returns>
        Rect GetCurrentlyPlayedMapObjectByCoordinates(int i, int j);

        /// <summary>
        /// Get the enum property of the  MapObject by coodinates to draw it out.
        /// </summary>
        /// <param name="i">X dimension.</param>
        /// <param name="j">Y dimension.</param>
        /// <returns>Property string.</returns>
        string GetEmumNameOfCurrentlyPlayedMapObjectByCoordinates(int i, int j);

        /// <summary>
        /// Gets all frames of a MapObject picture.
        /// </summary>
        /// <param name="temp">Property of MapObject.</param>
        /// <returns>List of URLs of the frames.</returns>
        List<string> ScanAllFramesOfGif(string temp);
    }
}
