﻿// <copyright file="IUserViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.ViewModel
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface of the User View Model.
    /// </summary>
    public interface IUserViewModel
    {
        /// <summary>
        /// Gets or sets the user we want to play with.
        /// </summary>
        string ChosenUser { get; set; }

        /// <summary>
        /// Gets or sets all existing usernames.
        /// </summary>
        List<string> Usernames { get; set; }

        /// <summary>
        /// Gets or sets which existing user have we choosen.
        /// </summary>
        string FromListChosenUser { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether hAve we choose an existing user or a new one.
        /// </summary>
        /// <returns>Was it successfull or not.</returns>
        bool FromListChosenUserBool { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a new user has heeb chosen.
        /// </summary>
        /// <returns>Was it successfull or not.</returns>
        bool BoolchosenUser { get; set; }

        /// <summary>
        /// Check whether user exists or not.
        /// </summary>
        /// <returns>Was it successfull or not.</returns>
        bool CheckUserExists();

        /// <summary>
        /// Add a new highscore.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <param name="mapid">MAp that the user played on.</param>
        /// <param name="time">The time the user finished the map.</param>
        /// <returns>Was it successfull or not.</returns>
        bool Add(string name, int mapid, int time);
    }
}
