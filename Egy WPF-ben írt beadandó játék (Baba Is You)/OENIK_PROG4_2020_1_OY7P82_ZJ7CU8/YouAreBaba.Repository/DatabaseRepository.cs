﻿// <copyright file="DatabaseRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Repositorys
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using YouAreBaba.Model;

    /// <summary>
    /// User data repository.
    /// </summary>
    public class DatabaseRepository : IDatabaseRepository
    {
        private XDocument allhighscore;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseRepository"/> class.
        /// </summary>
        public DatabaseRepository()
        {
            this.allhighscore = XDocument.Load(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
        }

        /// <summary>
        /// Get list of user data.
        /// </summary>
        /// <returns>The list of user data.</returns>
        public List<Highscore> Get()
        {
            var temper = new List<Highscore>();
            var dabber = this.allhighscore.Element("Highscore").Elements("User").ToList();
            foreach (var x in dabber)
            {
                var tew = Convert.ToInt32(x.Element("MapID").Value);
                temper.Add(new Highscore()
                {
                    Name = x.Element("Name").Value,
                    Mapid = tew,
                    Time = Convert.ToInt32(x.Element("Time").Value),
                });
            }

            return temper;
        }

        /// <summary>
        /// Add a new user highscore.
        /// </summary>
        /// <param name="name">Name of the user.</param>
        /// <param name="mapid">ID of the map.</param>
        /// <param name="time">Played time.</param>
        /// <returns>Success or not.</returns>
        public bool Add(string name, int mapid, int time)
        {
            var wastherelikethis = this.allhighscore.Element("Highscore").Elements("User").Where(x => x.Element("Name").Value.Equals(name)).Where(y => Convert.ToInt32(y.Element("MapID").Value) == mapid);
            if (wastherelikethis.Count() == 0)
            {
                XElement emp = new XElement(
                    "User",
                    new XElement("Name", name),
                    new XElement("MapID", mapid),
                    new XElement("Time", time));
                this.allhighscore.Element("Highscore").Add(emp);
                this.allhighscore.Save(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
                return true;
            }
            else if (wastherelikethis.Count() > 0)
            {
                wastherelikethis.First().Element("Time").SetValue(time);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Deletes all highscores for the user.
        /// </summary>
        /// <param name="name">Name of user.</param>
        /// <returns>Success or not.</returns>
        public bool DeleteAllRecord(string name)
        {
            this.allhighscore.Element("Highscore").Elements("User").Where(x => x.Element("Name").Value.Equals(name)).Remove();
            this.allhighscore.Save(@"../../../YouAreBaba.Repository/Highscore/Highscores.xml");
            return true;
        }
    }
}
