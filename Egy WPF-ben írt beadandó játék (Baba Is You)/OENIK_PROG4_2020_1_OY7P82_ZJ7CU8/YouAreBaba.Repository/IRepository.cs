﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Repositorys
{
    using System.Collections.Generic;
    using YouAreBaba.Model;
    using YouAreBaba.Model.Objects;

    /// <summary>
    /// The main repository class for map stuff.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Scann all the possible names of MapObject pictures.
        /// </summary>
        /// <returns>List of pic names.</returns>
        List<string> ScanAllPictureNames();

        /// <summary>
        /// Scan all map files.
        /// </summary>
        /// <returns>List of maps.</returns>
        List<List<MapObject>> ScanAllMaps();

        /// <summary>
        /// Scann all frames of a MapObject.
        /// </summary>
        /// <param name="temp">The property of MapObject.</param>
        /// <returns>The frames.</returns>
        List<string> ScanAllFramesOfGif(string temp);

        /// <summary>
        /// Add a quicksave.
        /// </summary>
        /// <param name="currentMap">Currently active map ID.</param>
        /// <param name="currentlyPlayedMap">Currently active map.</param>
        void AddSaveGame(int currentMap, List<MapObject> currentlyPlayedMap);

        /// <summary>
        /// Scan all map sizes.
        /// </summary>
        /// <returns>List of map data.</returns>
        List<int[]> ScanMapSizes();

        /// <summary>
        /// Quickload a map.
        /// </summary>
        /// <returns>The saved map.</returns>
        List<MapObject> LoadGame();
    }
}
