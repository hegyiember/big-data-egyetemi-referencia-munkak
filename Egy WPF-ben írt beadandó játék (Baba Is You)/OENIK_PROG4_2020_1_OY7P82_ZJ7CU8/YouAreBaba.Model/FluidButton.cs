﻿// <copyright file="FluidButton.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// The buttons in the MapChooser menu.
    /// </summary>
    public class FluidButton
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FluidButton"/> class.
        /// </summary>
        /// <param name="content">The text of the button.</param>
        /// <param name="leftPos">X coordinate.</param>
        /// <param name="topPos">Y coordinate.</param>
        /// <param name="productId">The ID of the map it represents.</param>
        public FluidButton(string content, double leftPos, double topPos, double productId)
        {
            this.Content = content;
            this.LeftPos = leftPos;
            this.TopPos = topPos;
            this.ProductId = productId;
            this.Background = Brushes.Transparent;
        }

        /// <summary>
        /// Gets or sets the color of its background.
        /// </summary>
        public Brush Background { get; set; }

        /// <summary>
        /// Gets or sets the text of the button.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets x coordinate.
        /// </summary>
        public double LeftPos { get; set; }

        /// <summary>
        /// Gets or sets y coordinate.
        /// </summary>
        public double TopPos { get; set; }

        /// <summary>
        /// Gets or sets the ID of the map it represents.
        /// </summary>
        public double ProductId { get; set; }

        /// <summary>
        /// Gets position setter.
        /// </summary>
        public Thickness ControlMargin
        {
            get { return new Thickness { Left = this.LeftPos, Top = this.TopPos }; }
        }
    }
}
