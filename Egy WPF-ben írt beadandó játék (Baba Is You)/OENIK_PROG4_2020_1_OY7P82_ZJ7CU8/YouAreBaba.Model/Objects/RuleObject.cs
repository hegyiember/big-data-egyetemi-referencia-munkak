﻿// <copyright file="RuleObject.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model.Objects
{
    /// <summary>
    /// A possible rule presented on the map by the proper MapObjects.
    /// </summary>
    public class RuleObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RuleObject"/> class.
        /// </summary>
        /// <param name="wORD">The WORD MapObject.</param>
        /// <param name="operator">The OPERATOR MapObject.</param>
        /// <param name="property">The Property MapObject.</param>
        /// <param name="vhe">Is the previous three vertical or hoprizontal to each other.</param>
        public RuleObject(MapObjectEnum wORD, MapObjectEnum @operator, MapObjectEnum property, VerticalHorizontalEnum vhe)
        {
            this.WORD = wORD;
            this.Operator = @operator;
            this.Property = property;
            this.VHE = vhe;
        }

        /// <summary>
        /// Gets or sets the WORD MapObject.
        /// </summary>
        public MapObjectEnum WORD { get; set; }

        /// <summary>
        /// Gets or sets the OPERATOR MapObject.
        /// </summary>
        public MapObjectEnum Operator { get; set; }

        /// <summary>
        /// Gets or sets the Property MapObject.
        /// </summary>
        public MapObjectEnum Property { get; set; }

        /// <summary>
        /// Gets or sets the previous three vertical or hoprizontal to each other.
        /// </summary>
        public VerticalHorizontalEnum VHE { get; set; }
    }
}
