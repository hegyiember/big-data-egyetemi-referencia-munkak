﻿// <copyright file="Highscore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace YouAreBaba.Model
{
    /// <summary>
    /// A highscore object.
    /// </summary>
    public class Highscore
    {
        /// <summary>
        /// Gets or sets gamer name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// </summary>
        public int Mapid { get; set; }

        /// <summary>
        /// Gets or sets the time the user completed the map.
        /// </summary>
        public int Time { get; set; }
    }
}
