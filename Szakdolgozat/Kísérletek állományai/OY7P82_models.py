########################################################################################################################
###     IMPORTS                                                                                                        #
########################################################################################################################

import math
import csv as csv
import sys
import numpy as np
from math import sqrt
from pandas import read_csv
import pandas as pd
from sklearn.metrics import mean_squared_error, accuracy_score
from scipy.stats import pearsonr
from tensorflow.python import confusion_matrix
from tensorflow.python.keras import Sequential, Input, Model
from tensorflow.python.keras.backend import concatenate
from tensorflow.python.keras.layers import Conv1D, MaxPooling1D, Flatten, Dense, Bidirectional, LSTM
import onnx
import keras2onnx

########################################################################################################################
###     SIDE FUNCTIONS                                                                                                 #
########################################################################################################################

def Calculate_P_values(train):
    df = pd.DataFrame(train)
    df = df.dropna()._get_numeric_data()
    dfcols = pd.DataFrame(columns=df.columns)
    pvalues = dfcols.transpose().join(dfcols, how='outer')
    for r in df.columns:
        for c in df.columns:
            pvalues[r][c] = round(pearsonr(df[r], df[c])[1], 4)
    return pvalues

def Calculate_Confusion_Matrix(y_test, y_pred):
    confusionmatrix = confusion_matrix(y_test, y_pred)
    accuracyscore = accuracy_score(y_test, y_pred)
    return confusionmatrix, accuracyscore

def Calculate_Neural_Network_model_structure(model):
    from keras_sequential_ascii import keras2ascii
    keras2ascii(model)

# make a forecast
def CNN_forecast(model, history, n_input):
    # flatten data
    data = np.array(history)
    data = data.reshape((data.shape[0] * data.shape[1], data.shape[2]))
    # retrieve last observations for input data
    input_x = data[-n_input:, :]
    # reshape into n input arrays
    input_x = [input_x[:, i].reshape((1, input_x.shape[0], 1)) for i in range(input_x.shape[1])]
    # forecast the next week
    yhat = model.predict(input_x, verbose=1)
    # itt visszaadjuk mind az x óra jósolt értékét ami keletkezett
    yhat = yhat[0]
    return yhat

def ConverterStatisticalRegressionsMenu(chosenweatherinput, chosenmodelinput):
    if chosenmodelinput == 'A' : chosenmodel = 'MLR'
    elif chosenmodelinput == 'B' : chosenmodel = 'RandomForestRegression'
    elif chosenmodelinput == 'C' : chosenmodel = 'SupportVectorRegression'
    else : chosenmodel = 'ERROR'
    if chosenweatherinput == 'hum' : chosenweather = 'Humidity'
    elif chosenweatherinput == 'pre' : chosenweather = 'Pressure'
    elif chosenweatherinput == 'win' : chosenweather = 'WindSpeed'
    elif chosenweatherinput == 'cc' : chosenweather = 'CloudCover'
    else : chosenweather = 'ERROR'
    return chosenweather, chosenmodel

def ConverterClassificationsMenu(chosenmodelinput):
    if chosenmodelinput == 'A' : chosenmodel = 'RandomForestClassification'
    elif chosenmodelinput == 'B' : chosenmodel = 'SupportVectorMachine'
    elif chosenmodelinput == 'C' : chosenmodel = 'GaussianNaiveBayes'
    elif chosenmodelinput == 'D' : chosenmodel = 'KNearestNeighbors'
    else : chosenmodel = 'ERROR'
    return chosenmodel

def ConverterNeuralNetworkMenu(chosenweatherinput, chosenmodelinput):
    if chosenmodelinput == 'A' : chosenmodel = 'CNN'
    elif chosenmodelinput == 'B' : chosenmodel = 'LTSM'
    else : chosenmodel = 'ERROR'
    if chosenweatherinput == 'tem' : chosenweather = 'Temperature'
    elif chosenweatherinput == 'dp' : chosenweather = 'DewPoint'
    else : chosenweather = 'ERROR'
    return chosenweather, chosenmodel

########################################################################################################################
###     MAIN FUNCTIONS                                                                                                 #
########################################################################################################################

def Read_input(chosenweather, chosenmodel, hours):
    identificationOfCurrentExperiment = chosenweather + '_' + chosenmodel + '_' + str(hours) + '_hour_'
    return identificationOfCurrentExperiment

def Read_dataset(hours, chosenweather):
    X_dataset = read_csv('X_dataset_2005_2015_' + chosenweather + '.csv',
                         encoding="ISO-8859-1", header=0, low_memory=False,
                         infer_datetime_format=True, parse_dates=['Date'],
                         index_col=['Date'])
    Y_dataset = read_csv('Y_dataset_2016_' + chosenweather + '.csv',
                         encoding="ISO-8859-1", header=0, low_memory=False,
                         infer_datetime_format=True, parse_dates=['Date'],
                         index_col=['Date'])
    # Only read the necessary legth (divideable with hours)
    X_dataset_rowlength = math.floor(X_dataset.shape[0] / hours) * hours
    Y_dataset_rowlength = math.floor(Y_dataset.shape[0] / hours) * hours
    # make it numerical
    X_dataset = X_dataset.astype('float32')
    Y_dataset = Y_dataset.astype('float32')
    X_dataset = X_dataset[:X_dataset_rowlength]
    Y_dataset = X_dataset[:Y_dataset_rowlength]
    return X_dataset, Y_dataset, X_dataset_rowlength, Y_dataset_rowlength

def Remake_data_for_usage_Neural_Network(X_dataset, Y_dataset, hours, X_dataset_rowlength, Y_dataset_rowlength):
    # put it out as 2D ndarray
    train, test = X_dataset[:], Y_dataset[:]
    train = np.array(np.split(train, X_dataset_rowlength / hours))  # len(train) 52 ha 2005-ből dolgozol
    test = np.array(np.split(test, Y_dataset_rowlength / hours))  # len(test)
    n_input = hours
    n_out = hours
    # flatten data
    data = train.reshape((train.shape[0] * train.shape[1], train.shape[2]))
    X, y = list(), list()
    in_start = 0
    # step over the entire history one time step at a time
    for _ in range(len(data)):
        # define the end of the input sequence
        in_end = in_start + n_input
        out_end = in_end + n_out
        # ensure we have enough data for this instance
        if out_end <= len(data):
            X.append(data[in_start:in_end, 1:])
            y.append(data[in_end:out_end, 0])
        # move along one time step
        in_start += 24
    train_x, train_y = np.array(X), np.array(y)
    return train_x, train_y, train, test

def Remake_data_for_usage_Regression_and_Classification_Models(X_dataset, Y_dataset):
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)
    X_train = X_dataset.iloc[:, :-1].values
    X_test = X_dataset.iloc[:, X_dataset.shape[1] - 1].values
    y_train = Y_dataset.iloc[:, :-1].values
    y_test = Y_dataset.iloc[:, X_dataset.shape[1] - 1].values
    return X_train, X_test, y_train, y_test

def Model_CNN(train_x, train_y):
    n_timesteps, n_features, n_outputs = train_x.shape[1], train_x.shape[2], train_y.shape[1]
    verbose, epochs, batch_size = 1, 25, 16
    in_layers, out_layers = list(), list()
    for i in range(n_features):
        inputs = Input(shape=(n_timesteps, 1))
        conv1 = Conv1D(filters=32, kernel_size=3, activation='relu')(inputs)
        conv2 = Conv1D(filters=32, kernel_size=3, activation='relu')(conv1)
        pool1 = MaxPooling1D(pool_size=2)(conv2)
        flat = Flatten()(pool1)
        in_layers.append(inputs)
        out_layers.append(flat)
    merged = concatenate(out_layers)
    dense1 = Dense(n_outputs * 4, activation='relu')(merged)
    dense2 = Dense(n_outputs * 2, activation='relu')(dense1)
    outputs = Dense(n_outputs)(dense2)
    model = Model(inputs=in_layers, outputs=outputs)
    model.compile(loss='mse', optimizer='adam')
    input_data = [train_x[:, :, i].reshape((train_x.shape[0], n_timesteps, 1)) for i in range(n_features)]
    model.fit(input_data, train_y, epochs=epochs, batch_size=batch_size, verbose=verbose, validation_split=0.087873)
    return model

def Model_LTSM(train_x, train_y, hours):
    n_input = hours
    n_timesteps, n_features, n_outputs = train_x.shape[1], train_x.shape[2], train_y.shape[1]
    model = Sequential()
    model.add(Bidirectional(LSTM(n_input, activation='relu'), input_shape=(n_timesteps, n_features)))
    model.add(Dense(n_outputs))
    model.compile(optimizer='adam', loss='mae')
    model.fit(train_x, train_y, epochs=50, verbose=1, validation_split=0.087873)
    return model

def Model_MultipleLinearRegression(X_train, X_test):
    from sklearn.linear_model import LinearRegression
    model = LinearRegression()
    model.fit(X_train, X_test)
    return model

def Model_RandomForestRegression(X_train, X_test):
    from sklearn.ensemble import RandomForestRegressor
    X_test = X_test.reshape(len(X_test), 1)
    model = RandomForestRegressor(n_estimators=100, random_state=0)
    model.fit(X_train, np.ravel(X_test))
    return model

def Model_SupportVectorRegression(X_train, X_test):
    from sklearn.svm import SVR
    model = SVR(kernel='rbf', verbose=True, cache_size=7000)
    model.fit(X_train, np.ravel(X_test))
    return model

def Model_SupportVectorClassification(X_train, X_test):
    from sklearn.svm import SVC
    model = SVC(kernel='linear', random_state=0)
    model.fit(X_train, X_test)
    return model

def Model_RandomForesClassification(X_train, X_test):
    from sklearn.ensemble import RandomForestClassifier
    model = RandomForestClassifier(n_estimators=5, criterion='entropy', random_state=0)
    model.fit(X_train, X_test)
    return model

def Prediction_Neural_Network(model, train, test, n_input):
    train2 = np.delete(train, 0, axis=2)
    # history is a list of weekly data
    history = [x for x in train2]
    # walk-forward validation over each week
    predictions = list()
    for i in range(len(test)):
        # Generates the exact same amount of predicted values as n_input
        yhat_sequence = CNN_forecast(model, history, n_input)
        # store the predictions
        predictions.append(yhat_sequence)
        # get real observation and add to history for predicting the next week
        history.append(test[i, :, 1:])
    # evaluate predictions days for each week
    predictions = np.array(predictions)
    actual = test[:, :, 0]
    return actual, predictions

def Prediction_Other_Models(model, y_train, y_test):
    y_pred = model.predict(y_train)
    np.set_printoptions(precision=2)
    scores = np.concatenate((y_pred.reshape(len(y_pred), 1), y_test.reshape(len(y_test), 1)), 1)
    return scores, y_pred

def RMSE_Neural_Networks_WriteintoFile(identificationOfCurrentExperiment, predicted, actual, n_out):
    scores = list()
    # calculate an RMSE score for each day
    for i in range(actual.shape[1]):
        # calculate mse
        mse = mean_squared_error(actual[:, i], predicted[:, i])
        # calculate rmse
        rmse = sqrt(mse)
        # store
        scores.append(rmse)
    # calculate overall RMSE
    s = 0
    for row in range(actual.shape[0]):
        for col in range(actual.shape[1]):
            s += (actual[row, col] - predicted[row, col]) ** 2
    overall_score = sqrt(s / (actual.shape[0] * actual.shape[1]))
    s_scores = ', '.join(['%.1f' % s for s in scores])
    print('%s: [%.3f] %s \n' % ('CNN RMSE', overall_score, s_scores))

    with open('RMSE/' + identificationOfCurrentExperiment + 'RMSE_output.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow('scores,average score,' + str(overall_score))
        for x in range(n_out):
            spamwriter.writerow(str(scores[x]))

    with open('Predictions/' + identificationOfCurrentExperiment + 'predicted_actual_output.csv', 'w', newline='') as csvfile:
        spamwriter2 = csv.writer(csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter2.writerow('actual,predicted')
        for x in range(len(actual)):
            for y in range(len(actual[0])):
                spamwriter2.writerow(str(actual[x][y]) + ',' + str(predicted[x][y]))

def RMSE_Others_WriteintoFile(scores, identificationOfCurrentExperiment):
    stringout = ""
    for x in scores:
        stringout += "\n"
        for y in x:
            stringout += str(y)
            stringout += ","
    original_stdout = sys.stdout
    with open('Predictions/' + identificationOfCurrentExperiment + 'predicted_actual_output.csv', 'w') as f:
        sys.stdout = f  # Change the standard output to the file we created.
        print(stringout)
        sys.stdout = original_stdout  # Reset the standard output to its original value

def ONNX_model_save(identificationOfCurrentExperiment, model):
    onnx_model_name = 'Models/ONXX_model_' + identificationOfCurrentExperiment + '.onnx'
    onnx_model = keras2onnx.convert_keras(model, model.name)
    onnx.save_model(onnx_model, onnx_model_name)

########################################################################################################################
###     MAIN                                                                                                           #
########################################################################################################################

print('Experiment subject: (1 - Statistical Regressions; 2 - Classifications; 3 - Neural Networks')
chosenexperimentinput = input()
if chosenexperimentinput == '1' :
    print('Enter chosen model: (A - MLR; B - Random Forest Regression; C - Support Vector Regression (SVR))')
    chosenmodelinput = input()
    print('Enter chosen weather: (hum - Humidity; pre - Pressure; win - WindSpeed; cc - CloudCover)')
    chosenweatherinput = input()
    chosenweather, chosenmodel = ConverterStatisticalRegressionsMenu(chosenweatherinput, chosenmodelinput)

    identificationOfCurrentExperiment = Read_input(chosenweather, chosenmodel, 168)
    X_dataset, Y_dataset, X_dataset_rowlength, Y_dataset_rowlength = Read_dataset(24, chosenweather)
    X_train, X_test, y_train, y_test = Remake_data_for_usage_Regression_and_Classification_Models(X_dataset, Y_dataset)

    if chosenmodelinput == 'A':
        model = Model_MultipleLinearRegression(X_train,X_test)
    elif chosenmodelinput == 'B':
        model = Model_RandomForestRegression(X_train,X_test)
    elif chosenmodelinput == 'C':
        model = Model_SupportVectorRegression(X_train,X_test)
    else:
        print('Wrong input!')

    scores, y_pred = Prediction_Other_Models(model, y_train, y_test)
    RMSE_Others_WriteintoFile(scores, identificationOfCurrentExperiment)
    # ONNX_model_save(identificationOfCurrentExperiment, model)

elif chosenexperimentinput == '2' :
    print('Enter chosen model: (A - Random Forest Classification; B - Support Vector Machine (SVM)')
    chosenmodelinput = input()
    chosenweather = 'daily_Precipation'
    chosenmodel = ConverterClassificationsMenu(chosenmodelinput)

    identificationOfCurrentExperiment = Read_input(chosenweather, chosenmodel, 168)
    X_dataset, Y_dataset, X_dataset_rowlength, Y_dataset_rowlength = Read_dataset(24, chosenweather)
    X_train, X_test, y_train, y_test = Remake_data_for_usage_Regression_and_Classification_Models(X_dataset, Y_dataset)

    if chosenmodelinput == 'A':
        model = Model_RandomForesClassification(X_train,X_test)
    elif chosenmodelinput == 'B':
        model = Model_SupportVectorClassification(X_train,X_test)
    else:
        print('Wrong input!')

    scores, y_pred = Prediction_Other_Models(model, y_train, y_test)
    Calculate_Confusion_Matrix(y_test, y_pred)
    RMSE_Others_WriteintoFile(scores, identificationOfCurrentExperiment)
    # ONNX_model_save(identificationOfCurrentExperiment, model)

elif chosenexperimentinput == '3' : #3
    print('Enter chosen model: (A - CNN; B - LTSM)')
    chosenmodelinput = input()
    print('Enter chosen weather: (tem - Temperature; dp - DewPoint)')
    chosenweatherinput = input()
    print('Prease insert the experiment window length (hours; must be dividable with 24)')
    hours = int(input())
    chosenweather, chosenmodel = ConverterNeuralNetworkMenu(chosenweatherinput, chosenmodelinput)

    identificationOfCurrentExperiment = Read_input(chosenweather, chosenmodel, hours)
    X_dataset, Y_dataset, X_dataset_rowlength, Y_dataset_rowlength = Read_dataset(24, chosenweather)
    X_train, X_test, train, test = Remake_data_for_usage_Neural_Network(X_dataset, Y_dataset, hours, X_dataset_rowlength, Y_dataset_rowlength)

    if chosenmodelinput == 'A':
        model = Model_CNN(X_train, X_test)
    elif chosenmodelinput == 'B':
        model = Model_LTSM(X_train, X_test, hours)
    else:
        print('Wrong input!')

    actual, predictions = Prediction_Neural_Network(model, train, test, hours)
    RMSE_Neural_Networks_WriteintoFile(identificationOfCurrentExperiment, predictions, actual, hours)
    # ONNX_model_save(identificationOfCurrentExperiment, model)
    # Calculate_Neural_Network_model_structure(model)
else:
    print('Wrong input!')