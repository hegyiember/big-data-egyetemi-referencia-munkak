# A repository t�m�ja #

2016 �sz�t�l kezd�d�, az �budai Egyetem NIK kar�n, Big Data �s �zleti intelligencia specializ�ci�n folytatott tanulm�nyomaim sor�n beadand�k �s �rai munk�k �ltal keletkezett, a Big Data ter�let�n szerzett ismereteimet megfelel�en reprezent�l� munk�im, illetve az elk�sz�lt �s sikeresen megv�dett szakdolgozatom. 

### Mely tant�rgyak t�m�it �rinti a repository tartalma? ###

* Korszer� adatb�zisok	 
* Adatt�rh�zak �s �zleti intelligencia
* Projektmunka I.
* Projektmunka II. 
* Szakdolgozat I.
* Szakdolgozat II.


### Milyen t�m�kat �rintenek a munk�im? ###

* Oracle SQL adatb�zisok fel�p�t�se, karbantart�sa  
* Halad� szint� lek�rdez�sek (aggreg�ci�, be�gyazott lek�rdez�sek, stb)
* Lek�rdez�s optimaliz�ci�
* NoSQL adatb�zisok �s nyelvek alkalmaz�sa (kiemelten MongoDB)
* Adatt�rh�z tervez�se �s elemeinek fel�p�t�se	
* Szakdolgozat: mesters�ges intelligencia felhaszn�l�sa predikci�s feladatok eset�n